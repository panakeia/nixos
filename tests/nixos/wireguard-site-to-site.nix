{ inputs, ... }:
{
  name = "wireguard-site-to-site";

  nodes =
    let
      commonSettings = {
        _module.args.inputs = inputs;
        imports = [ inputs.self.nixosModules.networking.wireguard-site-to-site ];
        networking.useDHCP = false;
        custom.networking.wireguard-site-to-site = {
          enable = true;
          networkName = "test0";
          domain = "test.local";
          setPrivateKeyFile = false;
        };
      };
    in
    {
      server = commonSettings // {
        networking = {
          interfaces.eth1.ipv4.addresses = [
            {
              address = "192.168.0.1";
              prefixLength = 24;
            }
          ];
          firewall.allowedUDPPorts = [ 51820 ];
          wg-quick.interfaces.test0.privateKey = "4BMXqbEPka86zT7dz8CQQsQ/KhQTXLYqhgjBFTHftms=";
        };

        custom.networking.wireguard-site-to-site =
          commonSettings.custom.networking.wireguard-site-to-site
          // {
            enableServer = true;
          };
      };

      client1 = commonSettings // {
        networking = {
          interfaces.eth1.ipv4.addresses = [
            {
              address = "192.168.0.2";
              prefixLength = 24;
            }
          ];
          wg-quick.interfaces.test0.privateKey = "yC5EjVvke9Vglfu8JHGI+pjNMoHeJZ8nG9e7sWnBuXY=";
        };
      };

      client2 = commonSettings // {
        networking = {
          interfaces.eth1.ipv4.addresses = [
            {
              address = "192.168.0.3";
              prefixLength = 24;
            }
          ];
          wg-quick.interfaces.test0.privateKey = "KBqOZVOjxTXbz2Gs2QND5a1LKyLzHcobmGVzs37/dWw=";
        };
      };
    };

  # TODO: Add subtest for DNS
  testScript = ''
    start_all()

    # load wireguard module
    server.succeed("modprobe wireguard")
    client1.succeed("modprobe wireguard")
    client2.succeed("modprobe wireguard")

    # wait for interfaces
    server.wait_for_unit("wg-quick-test0.service")
    client1.wait_for_unit("wg-quick-test0.service")
    client2.wait_for_unit("wg-quick-test0.service")

    with subtest("peers can reach each other"):
        # client -> server
        client1.succeed("ping -c5 10.1.0.1")
        client2.succeed("ping -c5 10.1.0.1")

        # server -> client
        server.succeed("ping -c5 10.1.1.1")
        server.succeed("ping -c5 10.1.1.2")

        # client -> client
        client1.succeed("ping -c5 10.1.1.2")
        client2.succeed("ping -c5 10.1.1.1")
  '';
}
