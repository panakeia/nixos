let
  host = "google.com";
  badHost = "doubleclick.net";
in
{ pkgs, inputs, ... }:
{
  name = "unbound-dns-blocking";

  nodes =
    let
      commonSettings = {
        _module.args.inputs = inputs;
        imports = [ inputs.self.nixosModules.networking.unbound-dns-blocking ];
        networking.useDHCP = false;
        networking.resolvconf.useLocalResolver = true;
        services.unbound.enable = true;
      };
    in
    {
      server = commonSettings // {
        networking.interfaces.eth1.ipv4.addresses = [
          {
            address = "192.168.0.1";
            prefixLength = 24;
          }
        ];

        environment.systemPackages = [ pkgs.dnsutils ];

        services.unbound = {
          enable = true;
          resolveLocalQueries = true;
          settings.forward-zone = [
            {
              name = ".";
              # TODO: replace with constant
              forward-addr = [ "9.9.9.9" ];
            }
          ];
        };

        custom.networking.unbound-dns-blocking = {
          enable = true;
          temporaryHosts = [ host ];
          blockOnCalendar = "Mon, 17:00";
          unblockOnCalendar = "Tue, 05:00";
        };
      };
    };

  testScript = ''
    start_all()

    server.wait_for_unit("network-online.target")
    server.wait_for_unit("unbound.service")

    # hosts are blocked when start-temporary-blocking runs
    server.succeed("systemctl start start-temporary-blocking.service")
    server.succeed("dig ${host} | grep NXDOMAIN")

    # hosts are unblocked when stop-temporary-blocking runs
    server.succeed("systemctl start stop-temporary-blocking.service")
    server.fail("dig ${host} | grep NXDOMAIN")

    # bad hosts are always blocked
    server.succeed("dig ${badHost} | grep NXDOMAIN")
  '';
}
