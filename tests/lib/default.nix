{ pkgs, lib }:

with lib;

runTests {
  testTrivial = {
    expr = 2 + 2;
    expected = 4;
  };

  testUnquote = {
    expr = custom.unquote ''"test"'';
    expected = "test";
  };

  testCombinePorts = {
    expr = custom.combinePorts (
      with custom.ports;
      [
        dns
        ssh
        steam
      ]
    );
    expected = {
      allowedUDPPorts = [ 53 ];
      allowedUDPPortRanges = [
        {
          from = 27031;
          to = 27036;
        }
      ];
      allowedTCPPorts = [
        22
        853
        27036
      ];
      allowedTCPPortRanges = [ ];
    };
  };

  testCombinePortsDedup = {
    expr = custom.combinePorts (
      with custom.ports;
      [
        dns
        wireguard
        wireguard
      ]
    );
    expected = {
      allowedUDPPorts = [
        53
        51820
      ];
      allowedUDPPortRanges = [ ];
      allowedTCPPorts = [ 853 ];
      allowedTCPPortRanges = [ ];
    };
  };

  testIsFree = {
    expr = [
      (custom.isFree pkgs.fleck)
      (custom.isFree pkgs.steam)
    ];
    expected = [
      true
      false
    ];
  };
}
