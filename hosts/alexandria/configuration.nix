{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

let
  inherit (builtins) attrValues;
  inherit (lib.custom) ports combinePorts;
  domain = "pan.ax";
in
{
  imports =
    (attrValues {
      inherit (inputs.nixos-hardware.nixosModules) common-cpu-amd common-pc-ssd;
    })
    ++ (attrValues {
      inherit (inputs.self.nixosModules.config)
        forgejo
        navidrome
        nginx
        radicale
        samba
        syncthing
        transmission
        unbound
        ;
    })
    ++ [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    # booting remotely:
    # nmap -sn <LAN subnet>
    # ssh -o IdentitiesOnly=true -o IdentityFile=/path/to/key # IP address from nmap
    # zfs load-key rpool
    # zpool import tank
    # zfs load-key tank
    # killall zfs && exit
    initrd = {
      # lspci -v | grep -iA8 'network\|ethernet'
      availableKernelModules = [ "r8169" ];
      network = {
        enable = true;
        ssh = {
          enable = true;
          authorizedKeys = map builtins.readFile [ "${inputs.self}/hosts/arcadia/ssh-public-keys/root.pub" ];
          # ssh-keygen -t rsa -N "" -f /persist/secrets/initrd/ssh_host_rsa_key
          # ssh-keygen -t ed25519 -N "" -f /persist/secrets/initrd/ssh_host_ed25519_key
          hostKeys = [
            "/persist/secrets/initrd/ssh_host_rsa_key"
            "/persist/secrets/initrd/ssh_host_ed25519_key"
          ];
        };
      };
    };
  };

  networking = {
    hostName = "alexandria";
    hostId = "f3beb63f";
    useNetworkd = false;
    interfaces = {
      enp7s0.useDHCP = true;
    };
    firewall = {
      interfaces = with ports; {
        enp7s0 = combinePorts [
          ssh
          https
          smb
          syncthing
          wireguard
        ];
        pan0 = combinePorts [
          dns
          ssh
          https
          smb
          syncthing
        ];
        cni-podman0 = combinePorts [ https ];
      };
    };
  };

  powerManagement.cpuFreqGovernor = "powersave";

  custom.networking.wireguard-site-to-site = {
    inherit domain;
    enable = true;
    enableServer = true;
    networkName = "pan0";
  };

  custom.profiles = {
    ephemeralRoot.enable = true;
    ssh.enable = true;
  };

  home-manager.users.pan = {
    home.stateVersion = "21.11";

    home.packages = lib.mkForce pkgs.metaPackages.cli.core;

    home.file = builtins.listToAttrs (
      map
        (name: {
          name = name;
          value = {
            source = config.home-manager.users.pan.lib.file.mkOutOfStoreSymlink "/srv/tank/${name}";
          };
        })
        [
          "Audio"
          "Documents"
          "Games"
          "Music"
          "Pictures"
          "Videos"
        ]
    );
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?
}
