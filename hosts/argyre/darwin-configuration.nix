{ pkgs, ... }:

{
  networking.hostName = "argyre";

  nix.settings = {
    # You should generally set this to the total number of logical cores in your system.
    # $ sysctl -n hw.ncpu
    max-jobs = 10;
    cores = 10;
  };

  # For the unsupported packages that build fine on macOS
  nixpkgs.config.allowUnsupportedSystem = true;

  environment.systemPackages = with pkgs; [ coreutils ];

  programs.gnupg.agent.enable = true;

  custom.profiles = {
    desktop.enable = true;
  };

  home-manager.users.pan = {
    home.stateVersion = "21.05";

    home.packages = pkgs.metaPackages.cli.all;
  };

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
}
