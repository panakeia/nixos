{ lib, inputs, ... }:

with lib;

let
  inherit (lib.custom) ports combinePorts;
in
{
  imports = with inputs.nixos-hardware.nixosModules; [
    common-cpu-intel
    common-pc-ssd
    common-pc-laptop

    # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  boot.loader.systemd-boot.enable = true;

  networking = {
    hostName = "lemnos";
    hostId = "f3e469f7";
    interfaces = {
      enp0s31f6.useDHCP = true;
      wlp4s0.useDHCP = true;
    };
    firewall.interfaces = with ports; {
      pan0 = combinePorts [ ssh ];
    };
  };

  users.mutableUsers = mkForce true;
  sops.secrets = mkForce { };

  custom.profiles = {
    hardened.enable = false;
    desktop.enable = true;
    minimalX11.enable = true;
    ephemeralRoot.enable = true;
  };

  home-manager.users.pan.home.stateVersion = "21.05";

  system.stateVersion = "21.05";
}
