# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:

let
  inherit (builtins) attrValues;
  inherit (lib.custom) ports combinePorts;
in
{
  imports =
    (attrValues {
      inherit (inputs.nixos-hardware.nixosModules)
        common-cpu-amd
        common-gpu-amd
        common-pc-ssd
        ;
    })
    ++ (attrValues {
      inherit (inputs.self.nixosModules.config) syncthing smb-client;
    })
    ++ [
      # Include disk configuration.
      ./disk.nix

      # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.systemd-boot.enable = true;

  networking = {
    hostName = "arcadia";
    hostId = "fc040bd9";
    interfaces = {
      enp5s0.useDHCP = true;
      wlp4s0.useDHCP = true;
    };
    firewall.interfaces = with ports; {
      enp5s0 = combinePorts [
        kdeconnect
        input-leap
        steam
        syncthing
      ];
      pan0 = combinePorts [
        ssh
        syncthing
      ];
    };
  };

  virtualisation.virtualbox.host.enable = true;

  custom.networking.wireguard-site-to-site = {
    enable = true;
    networkName = "pan0";
    domain = "pan.ax";
  };

  custom.profiles = {
    crossCompilation.enable = true;
    ephemeralRoot = {
      enable = true;
      zpool = "zroot";
    };
    kde.enable = true;
    gaming.enable = true;
    ssh.enable = true;
  };

  home-manager.users.pan = {
    home.stateVersion = "21.05";

    home.packages = lib.concatLists [
      pkgs.metaPackages.cli.all
      pkgs.metaPackages.gui.all
    ];

    wayland.windowManager.sway.config.output = {
      "DP-1" = {
        mode = "2560x1440@143.998Hz";
      };
    };
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "21.05"; # Did you read the comment?
}
