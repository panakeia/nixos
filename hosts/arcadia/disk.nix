{
  disko.devices = {
    disk = {
      back = {
        type = "disk";
        device = "/dev/disk/by-id/nvme-WD_BLACK_SN850X_8000GB_2445404A1J01";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              size = "512M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = [ "umask=0077" ];
              };
            };
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "zroot";
              };
            };
          };
        };
      };
      # front = {
      #   type = "disk";
      #   device = "/dev/sdy"; TODO: change
      #   content = {
      #     type = "gpt";
      #     partitions = {
      #       zfs = {
      #         size = "100%";
      #         content = {
      #           type = "zfs";
      #           pool = "zroot";
      #         };
      #       };
      #     };
      #   };
      # };
    };
    zpool = {
      zroot = {
        type = "zpool";
        # TODO: enable when the other disk is installed
        # mode = "mirror";
        rootFsOptions = {
          atime = "off";
          acltype = "posixacl";
          xattr = "sa";
          mountpoint = "none";
          compression = "zstd";
          encryption = "aes-256-gcm";
          keyformat = "passphrase";
          "com.sun:auto-snapshot" = "false";
        };
        postCreateHook = "zfs snapshot -r zroot@blank";

        datasets = {
          local.type = "zfs_fs";
          "local/root" = {
            type = "zfs_fs";
            options.mountpoint = "legacy";
            mountpoint = "/";
          };
          "local/nix" = {
            type = "zfs_fs";
            options.mountpoint = "legacy";
            mountpoint = "/nix";
          };
          safe.type = "zfs_fs";
          "safe/persist" = {
            type = "zfs_fs";
            options."com.sun:auto-snapshot" = "true";
            options.mountpoint = "legacy";
            mountpoint = "/persist";
          };
          "safe/home" = {
            type = "zfs_fs";
            options."com.sun:auto-snapshot" = "true";
            options.mountpoint = "legacy";
            mountpoint = "/home";
          };
          "safe/home/pan" = {
            type = "zfs_fs";
            options.mountpoint = "legacy";
            mountpoint = "/home/pan";
          };
        };
      };
    };
  };
}
