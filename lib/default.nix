lib:

let
  isFree =
    drv:
    let
      result = builtins.tryEval (
        lib.isDerivation drv
        && (
          !(lib.elem
            (lib.attrByPath [
              "meta"
              "license"
            ] null drv)
            [
              lib.licenses.unfree
              lib.licenses.unfreeRedistributable
            ]
          )
        )
      );
    in
    result.success && result.value;

  mkEnableModule =
    { config, lib, ... }:
    {
      path,
      settings,
      default ? false,
      ...
    }@args:
    let
      cfg = lib.attrByPath path { enable = false; } config;
      options = lib.setAttrByPath path {
        enable = lib.mkOption {
          inherit default;
          inherit (args) description;
          type = lib.types.bool;
        };
      };
    in
    {
      inherit options;
      config = lib.mkIf cfg.enable settings;
    };
in
{
  inherit isFree mkEnableModule;
  inherit (import ./constants.nix)
    projectName
    projectRootFile
    chromeExtensions
    dnsServers
    ports
    ;

  pickNewest =
    orig: mine: if (builtins.compareVersions orig.version mine.version) == -1 then mine else orig;

  mkMergeIf = condition: attrsList: lib.mkIf condition (lib.mkMerge attrsList);

  mkIfLazy = condition: attrs: if condition then attrs else { };

  # Create a module from a nixpkgs branch.
  mkNixpkgsBranchModule =
    {
      branch,
      system,
      pkgs,
      disable ? true,
      modules ? [ ],
      overlay ? null,
    }:
    let
      branchPkgs = import branch {
        inherit system;
        config.allowUnfree = true;
        overlays = [ (_: _: { inherit (pkgs) callPackage callPackages libsForQt5; }) ];
      };
    in
    {
      config,
      lib,
      pkgs,
      options,
      ...
    }:
    {
      disabledModules = lib.optionals disable modules;
      imports = map (
        module:
        (import "${branch}/nixos/modules/${module}" {
          inherit
            config
            lib
            pkgs
            options
            ;
        })
      ) modules;
      nixpkgs = {
        config.allowUnfree = true;
        overlays = lib.mkBefore (lib.optional (overlay != null) (overlay branchPkgs));
      };
    };

  mkProfile =
    { lib, ... }@moduleArgs:
    { name, ... }@args:
    mkEnableModule moduleArgs (
      lib.recursiveUpdate args {
        path = [
          "custom"
          "profiles"
          name
        ];
      }
    );

  combinePorts =
    services:
    let
      singlePort = port: builtins.typeOf port == "int";
      portRange =
        range:
        builtins.typeOf range == "set" && builtins.hasAttr "from" range && builtins.hasAttr "to" range;
      allow =
        protocol: fn:
        lib.sort (a: b: a < b) (
          lib.unique (
            lib.concatLists (
              map (
                service: if builtins.hasAttr protocol service then lib.filter fn service.${protocol} else [ ]
              ) services
            )
          )
        );
    in
    {
      allowedTCPPorts = allow "tcp" singlePort;
      allowedTCPPortRanges = allow "tcp" portRange;
      allowedUDPPorts = allow "udp" singlePort;
      allowedUDPPortRanges = allow "udp" portRange;
    };

  composeAttrs = lib.foldl' (lib.recursiveUpdate) { };

  associateMimeTypes =
    type: subtypes: desktopFile:
    builtins.listToAttrs (
      map (subtype: {
        name = "${type}/${subtype}";
        value = desktopFile;
      }) subtypes
    );

  mkSubdomainRecords =
    ipAddress: subdomains:
    builtins.listToAttrs (
      map (subdomain: {
        name = subdomain;
        value = {
          A = [ ipAddress ];
        };
      }) subdomains
    );

  dnsZoneToUnboundLocalData =
    zone:
    let
      inherit (builtins.elemAt zone.A 0) class;
      domain = builtins.elemAt zone.NS 0;
    in
    (map (record: ''"${domain} 10800 ${class} A ${record.address}"'') zone.A)
    ++ (lib.mapAttrsToList (
      name: value: ''"${name}.${domain} 10800 ${class} A ${builtins.elemAt value.A 0}"''
    ) zone.subdomains);

  unquote = str: lib.removeSuffix ''"'' (lib.removePrefix ''"'' str);

  canEval =
    hostPlatform: drv:
    lib.hasAttrByPath [
      "meta"
      "platforms"
    ] drv
    && lib.hasAttrByPath [
      "meta"
      "license"
    ] drv
    && lib.meta.availableOn hostPlatform drv
    && !(
      lib.hasAttrByPath [
        "meta"
        "broken"
      ] drv
      && drv.meta.broken
    )
    && isFree drv;
}
