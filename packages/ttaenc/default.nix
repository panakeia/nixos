{
  stdenv,
  lib,
  fetchurl,
}:

stdenv.mkDerivation rec {
  pname = "ttaenc";
  version = "3.4.1";

  src = fetchurl {
    url = "mirror://sourceforge/tta/tta/ttaenc-src/ttaenc-${version}-src.tgz";
    hash = "sha256:1iixpr4b89g9g1hwn8ak8k8iflcww3r5f09a117qdidc2nqcijdj";
  };

  installPhase = ''
    mkdir -p "$out/bin"
    cp ttaenc "$out/bin"
  '';

  meta = {
    platforms = lib.platforms.linux;
    license = lib.licenses.gpl2Plus;
  };
}
