{
  appimageTools,
  lib,
  fetchurl,
  makeDesktopItem,
}:

let
  version = "0.52.2";
  desktopItem = makeDesktopItem {
    name = "Sabaki";
    exec = "sabaki";
    desktopName = "sabaki";
  };
in
appimageTools.wrapType2 {
  name = "sabaki";
  inherit version;

  src = fetchurl {
    url = "https://github.com/SabakiHQ/Sabaki/releases/download/v${version}/sabaki-v${version}-linux-x64.AppImage";
    hash = "sha256:0inlp5wb8719qygcac5268afim54ds7knffp765csrfdggja7q62";
  };

  extraInstallCommands = ''
    mkdir -p $out/share/applications
    ln -s ${desktopItem}/share/applications/* $out/share/applications
  '';

  meta = {
    platforms = [ "x86_64-linux" ];
    license = lib.licenses.mit;
  };
}
