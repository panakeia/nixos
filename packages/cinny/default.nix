{
  stdenv,
  lib,
  npmlock2nix,
  fetchFromGitHub,
  python3,
  vips,
  glib,
  pkg-config,
  nodejs_20,
}:

npmlock2nix.build rec {
  pname = "cinny";
  version = "2.2.2";

  src = fetchFromGitHub {
    owner = "ajbura";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-VZvHeJxkR1MIQnYZGrR2DrPoHkIklbWGNBbbAhilFBY=";
  };

  nodejs = nodejs_20;

  # for sharp:
  # https://github.com/lovell/sharp/blob/master/docs/install.md#custom-libvips
  node_modules_attrs = {
    buildInputs = [
      python3
      vips
      glib
      pkg-config
    ];
  };

  installPhase = "cp -r dist $out/";

  meta = {
    platforms = lib.platforms.unix;
    license = lib.licenses.mit;
    broken = stdenv.hostPlatform.isAarch64; # due to libjxl via vips: https://github.com/libjxl/libjxl/issues/213
  };
}
