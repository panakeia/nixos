# NOTE: by default, the game looks for data files in its own directory. When it's
# located in the Nix store, this directory is read-only. There is a "-gamedir" flag
# which can be used like: `usc-game -gamedir=/path/to/gamedir` (note the single `-` and `=`)
# to work around this issue.
#
# The upstream .desktop file has been modified to pass -gamedir=$HOME/.local/share/usc-game
# by default.
#
# TODO: For the game to run, the contents of the `bin` directory from
# https://github.com/Drewol/unnamed-sdvx-clone will also need to be present in the gamedir.
# At present, these files are not included in the package.

{
  stdenv,
  lib,
  fetchgit,
  cmake,
  freetype,
  libogg,
  libvorbis,
  SDL2,
  zlib,
  libpng,
  libjpeg,
  libarchive,
  libGL,
  openssl,
  libiconv,
  curl,
  rapidjson,
}:

let
  pname = "unnamed-sdvx-clone";
  version = "0.5.0";
in
stdenv.mkDerivation {
  inherit pname version;

  src = fetchgit {
    url = "https://github.com/Drewol/unnamed-sdvx-clone.git";
    rev = "d748c188544820ab518821f3699df3a676cec68b";
    hash = "sha256-p4y0aGZKMByWjRHf/3Ro1EbTs8lIQTCES0VaNEd230c=";
    fetchSubmodules = true;
  };

  nativeBuildInputs = [ cmake ];

  buildInputs = [
    freetype
    libogg
    libvorbis
    SDL2
    zlib
    libpng
    libjpeg
    libarchive
    libGL
    openssl
    libiconv
    # cpr
    curl
    # discord-rpc
    rapidjson
  ];

  cmakeFlags = [
    # prevent cpr from trying to download curl
    "-DCPR_FORCE_USE_SYSTEM_CURL=ON"
  ];

  dontUseCmakeBuildDir = true;

  postInstall = ''
    mkdir -p $out/share/applications
    mkdir -p $out/share/pixmaps
    mkdir -p $out/bin
    cp appimage/usc-game.desktop $out/share/applications
    sed -i "s|Exec=usc-game|Exec=sh -c \"\\$out/bin/usc-game -gamedir\=\\\$HOME/.local/share/usc-game\"|" $out/share/applications/usc-game.desktop
    cp appimage/usc-game.png $out/share/pixmaps
    cp bin/usc-game $out/bin
  '';

  meta = {
    platforms = lib.platforms.linux;
    license = lib.licenses.mit;
    # vendored cpr version is affected by
    # https://github.com/libcpr/cpr/issues/870
    broken = true;
  };
}
