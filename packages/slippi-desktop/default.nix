{
  appimageTools,
  fetchurl,
  lib,
  gsettings-desktop-schemas,
  gtk3,
}:

appimageTools.wrapType2 rec {
  pname = "slippi-desktop";
  version = "2.8.1";

  src = fetchurl {
    url = "https://github.com/project-slippi/slippi-desktop-app/releases/download/v${version}/Slippi-Launcher-${version}-x86_64.AppImage";
    hash = "sha256-V+hFHWqj8S5r4yLbwP6zdFcYNGEiY6kwOhqrJxVhVak=";
  };

  profile = ''
    export LC_ALL=C.UTF-8
    export XDG_DATA_DIRS=${gsettings-desktop-schemas}/share/gsettings-schemas/${gsettings-desktop-schemas.name}:${gtk3}/share/gsettings-schemas/${gtk3.name}:$XDG_DATA_DIRS
  '';

  multiPkgs = null; # no 32bit needed
  extraPkgs = appimageTools.defaultFhsEnvArgs.multiPkgs;
  extraInstallCommands = "mv $out/bin/{${pname}-${version},${pname}}";

  meta = with lib; {
    description = "Slippi desktop app used for launching replays";
    homepage = "https://github.com/project-slippi/slippi-desktop-app";
    license = licenses.gpl3Only;
    platforms = [ "x86_64-linux" ];
  };
}
