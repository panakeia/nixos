{
  stdenv,
  mkYarnPackage,
  nodejs,
  autotable,
}:

stdenv.mkDerivation rec {
  pname = "autotableServer";
  version = autotable.version;

  src = "${autotable.src}/server";

  autotable-server-js-modules = mkYarnPackage {
    pname = "${pname}-js-modules";
    inherit src version;
  };

  nativeBuildInputs = [ nodejs ];

  buildPhase = ''
    ln -s ${autotable-server-js-modules}/libexec/*/node_modules node_modules
    npm run build
  '';

  installPhase = ''
    mkdir -p $out
    cp -R dist/* $out
  '';

  # TODO: should be able to build on lib.platforms.unix,
  # but mkYarnPackage eagerly tries to build the yarn.nix derivation
  # when the package is evaluated, leading to the following error
  # when running `nix flake check`:
  #
  # "a 'x86_64-darwin' with features {} is required to build yarn.nix.drv,
  # but I am a $PLATFORM with features {}"
  meta = autotable.meta // {
    description = "An online mahjong table (server)";
  };
}
