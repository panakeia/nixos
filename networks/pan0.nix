{
  peers = {
    servers = {
      alexandria = {
        publicKey = "0XNYyUDhNAIjbSAgSmrc1AygMyC+PJ3jND/u/N5PUWQ=";
        # TODO: Replace with FQDN
        endpoint = "192.168.1.69:51820";
        ip = "10.69.0.1";
        allowedIPs = [ "10.69.0.0/16" ];
        persistentKeepalive = 25;
      };
    };
    clients = {
      arcadia = {
        publicKey = "O6AC83MKoJSkYsKfItxfNdT0JTL30/9dzjgNF1s5JWk=";
        ip = "10.69.1.1";
      };
      argyre = {
        publicKey = "pFNzpZKOlzlZO+H5KcKM1fq68FNXgxU3lVWyuCB3Mhc=";
        ip = "10.69.1.2";
      };
      se = {
        publicKey = "hmY8HXgeuwBtqczYqNcoZlYQ0BnkBJcLqvfPeCyXK1o=";
        ip = "10.69.1.6";
      };
    };
  };
}
