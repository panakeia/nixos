{
  description = "NixOS system configurations, custom modules, overlays, package definitions, etc.";

  inputs = {
    nixpkgs-stable.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-unstable";
      follows = "nixpkgs-unstable";
    };

    # First-party inputs.
    # pan9-secrets = {
    #   type = "path";
    #   path = "/home/pan/src/pan9-secrets";
    #   flake = false;
    # };
    pan9-secrets = {
      type = "git";
      url = "git+ssh://git@gitlab.com/panakeia/pan9-secrets.git";
      flake = false;
    };
    "pan.ax" = {
      type = "git";
      url = "https://git.pan.ax/pan/pan.ax.git";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
      inputs.flake-parts.follows = "flake-parts";
    };

    # Inputs providing tooling related to flakes.
    deploy-rs = {
      url = "github:serokell/deploy-rs";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
      inputs.utils.follows = "flake-utils";
    };
    flake-parts.url = "github:hercules-ci/flake-parts";
    flake-root.url = "github:srid/flake-root";
    flake-utils.url = "github:numtide/flake-utils";
    mission-control.url = "github:Platonic-Systems/mission-control";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };

    # Inputs providing modules for NixOS and other systems (home-manager, nix-darwin).
    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.91.1-1.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    darwin = {
      url = "github:lnl7/nix-darwin/master";
      follows = "nix-darwin";
    };
    home-manager-stable = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs-stable";
    };
    home-manager-unstable = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    impermanence.url = "github:nix-community/impermanence";
    nix-darwin = {
      url = "github:lnl7/nix-darwin/master";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };

    # Inputs providing Nix packages and overlays.
    nix-index-database.url = "github:Mic92/nix-index-database";
    nur.url = "github:nix-community/NUR";

    # Inputs providing Nix libraries.
    dns = {
      url = "github:kirelagin/dns.nix";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
      inputs.flake-utils.follows = "flake-utils";
    };
    haumea = {
      url = "github:nix-community/haumea";
      inputs.nixpkgs.follows = "nixpkgs-lib";
    };
    nixpkgs-lib.url = "github:nix-community/nixpkgs.lib";
    npmlock2nix = {
      url = "github:tweag/npmlock2nix";
      flake = false;
    };

    # Miscellaneous.
    hosts = {
      url = "github:StevenBlack/hosts";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    inputs:
    let
      lib = inputs.nixpkgs-lib.lib.extend (_final: prev: { custom = import ./lib prev; });

      modules = inputs.haumea.lib.load {
        src = ./modules;
        loader = inputs.haumea.lib.loaders.verbatim;
      };
    in
    lib.recursiveUpdate
      (inputs.flake-parts.lib.mkFlake { inherit inputs; } {
        imports = [
          inputs.pre-commit-hooks.flakeModule
          inputs.flake-root.flakeModule
          inputs.mission-control.flakeModule
        ];

        systems = [
          "x86_64-linux"
          "aarch64-linux"
          "x86_64-darwin"
          "aarch64-darwin"
        ];

        flake =
          let
            nixpkgsConfig = {
              allowUnfree = true;
            };

            mkDarwinConfig =
              hostname:
              {
                system,
                nixpkgs ? inputs.nixpkgs-unstable,
              }:
              let
                pkgs = nixpkgs.legacyPackages.${system};
                evalConfig = import "${inputs.nix-darwin}/eval-config.nix" {
                  lib = lib.extend (
                    _final: prev: {
                      inherit (inputs.home-manager-stable.lib) hm;
                      custom = prev.recursiveUpdate prev.custom {
                        mkLinux = prev.custom.mkIfLazy pkgs.hostPlatform.isLinux;
                        mkDarwin = prev.custom.mkIfLazy pkgs.hostPlatform.isDarwin;
                      };
                    }
                  );
                };
              in
              evalConfig {
                inherit system;
                inputs = lib.recursiveUpdate inputs { inherit nixpkgs; };
                specialArgs = {
                  inherit inputs nixpkgsConfig;
                };
                modules = [
                  inputs.nix-darwin.darwinModules.flakeOverrides
                  inputs.home-manager-unstable.darwinModules.home-manager
                  inputs.self.darwinModules.default
                  (./hosts + "/${hostname}/darwin-configuration.nix")
                ];
              };

            mkConfig =
              hostname:
              {
                system,
                nixpkgs ? inputs.nixpkgs-stable,
                home-manager ? inputs.home-manager-stable,
              }:
              let
                pkgs = import nixpkgs {
                  inherit system;
                  config = nixpkgsConfig;
                };
              in
              # NOTE: makeOverridable is used so we can run nixos-generate
              # referencing the flake's nixosConfigurations attribute
              #
              # (refer: https://github.com/nix-community/nixos-generators/pull/76)
              lib.makeOverridable nixpkgs.lib.nixosSystem {
                inherit system;
                lib = pkgs.lib.extend (
                  _final: prev: {
                    inherit (inputs.home-manager-stable.lib) hm;
                    custom = prev.recursiveUpdate lib.custom {
                      mkLinux = lib.custom.mkIfLazy pkgs.hostPlatform.isLinux;
                      mkDarwin = lib.custom.mkIfLazy pkgs.hostPlatform.isDarwin;
                    };
                  }
                );
                specialArgs = {
                  inherit inputs nixpkgsConfig;
                };
                modules = [
                  nixpkgs.nixosModules.notDetected
                  inputs.lix-module.nixosModules.default
                  inputs.disko.nixosModules.default
                  inputs.sops-nix.nixosModules.sops
                  inputs.impermanence.nixosModules.impermanence
                  home-manager.nixosModules.home-manager
                  inputs.self.nixosModules.default
                  (./hosts + "/${hostname}/configuration.nix")
                ];
              };

            hosts = {
              arcadia.system = "x86_64-linux";
              alexandria.system = "x86_64-linux";
              lemnos.system = "x86_64-linux";
            };
          in
          {
            nixosConfigurations = lib.mapAttrs mkConfig hosts;

            darwinConfigurations = lib.mapAttrs mkDarwinConfig { argyre.system = "aarch64-darwin"; };

            sshHostKeys =
              lib.recursiveUpdate
                (lib.mapAttrs
                  (name: _: {
                    publicKeyFile = ./hosts + "/${name}/ssh-public-keys/ssh_host_ed25519_key.pub";
                  })
                  (
                    lib.filterAttrs (
                      name: _: builtins.pathExists (./hosts + "/${name}/ssh-public-keys/ssh_host_ed25519_key.pub")
                    ) inputs.self.nixosConfigurations
                  )
                )
                {
                  "alexandria" = {
                    publicKeyFile = ./hosts/alexandria/ssh-public-keys/ssh_host_ed25519_key.pub;
                    aliases = [ "git.pan.ax" ];
                  };
                };

            dnsZones =
              let
                inherit (inputs.self.wireguardNetworks.pan0.peers) clients servers;
                allPeers = servers // clients;
                domain = "pan.ax";
                publicIpAddress = "77.173.87.196";
                internalReverseProxyIpAddress = "10.69.0.1";
              in
              {
                "${domain}" = inputs.dns.lib.evalZone domain {
                  SOA = {
                    nameServer = "dns.${domain}.";
                    adminEmail = "pan@${domain}";
                    serial = 202202080000;
                  };
                  NS = [ "${domain}." ];
                  A = [ { address = "10.69.0.1"; } ];
                  subdomains =
                    {
                      gateway.A = [ publicIpAddress ];
                    }
                    // (lib.mapAttrs (_: value: { A = [ value.ip ]; }) allPeers)
                    // (lib.custom.mkSubdomainRecords internalReverseProxyIpAddress [
                      "dns"
                      "git"
                      "calendar"
                      "radio"
                      "torrents"
                      "ci"
                      "chat"
                      "passwords"
                      "wiki"
                      "music"
                    ]);
                };
              };

            wireguardNetworks = inputs.haumea.lib.load {
              src = ./networks;
              loader = inputs.haumea.lib.loaders.verbatim;
            };

            deploy = {
              sshUser = "pan";
              sshOpts = [ "-A" ];

              nodes =
                let
                  nodeNames = [ "alexandria" ];
                  mkNode = name: {
                    hostname = "${name}.pan.ax";
                    profiles.system = {
                      user = "root";
                      path =
                        inputs.deploy-rs.lib.${hosts.${name}.system}.activate.nixos
                          inputs.self.nixosConfigurations.${name};
                    };
                  };
                in
                builtins.listToAttrs (
                  map (name: {
                    inherit name;
                    value = mkNode name;
                  }) nodeNames
                );
            };

            # nix flake check wants to see explicit parameters for NixOS overlays
            overlays.default =
              final: prev:
              (lib.composeManyExtensions [
                (import ./overlays/remotePackages.nix {
                  inherit inputs nixpkgsConfig lib;
                })
                (import ./overlays/patches.nix)
                (import ./overlays/localPackages.nix)
              ])
                final
                prev;

            inherit lib;

            checks =
              lib.recursiveUpdate
                (builtins.mapAttrs (
                  _system: deployLib: deployLib.deployChecks inputs.self.deploy
                ) inputs.deploy-rs.lib)
                {
                  x86_64-linux =
                    let
                      system = "x86_64-linux";
                      pkgs = import inputs.nixpkgs-unstable {
                        inherit system;
                        config = nixpkgsConfig;
                        overlays = [ inputs.self.overlays.default ];
                      };
                      makeNixosTest = import "${inputs.nixpkgs-unstable}/nixos/tests/make-test-python.nix";
                    in
                    lib.mapAttrs'
                      (
                        name: value: lib.nameValuePair "nixos-${name}" (makeNixosTest value { inherit system pkgs inputs; })
                      )
                      (
                        inputs.haumea.lib.load {
                          src = ./tests/nixos;
                          loader = inputs.haumea.lib.loaders.verbatim;
                        }
                      );
                };
          };

        perSystem =
          { config, pkgs, ... }:
          let
            pkgs' = pkgs.extend (inputs.self.overlays.default);
            packages = lib.filterAttrs (_: lib.custom.canEval pkgs.stdenv.hostPlatform) pkgs'.localPackages;
          in
          {
            inherit packages;

            devShells.default = pkgs.mkShellNoCC {
              inputsFrom = [ config.mission-control.devShell ];
              shellHook = config.pre-commit.installationScript;
              buildInputs = with pkgs; [
                deadnix
                deploy-rs
                nil
                nixfmt-rfc-style
                nix-update
                nix-template
                (nixos-generators.override { inherit (pkgs) nix; })
                pre-commit
                shellcheck
                shfmt
                sops
                ssh-to-age
              ];
            };

            pre-commit.settings.hooks = {
              deadnix.enable = true;
              nixfmt-rfc-style.enable = true;
              shellcheck.enable = true;
              shfmt.enable = true;
            };

            flake-root.projectRootFile = lib.custom.projectRootFile;

            mission-control = {
              wrapperName = "p9";
              banner = "";
              scripts =
                let
                  runScriptInProjectRoot = script: ''
                    pushd "$(dirname "${lib.custom.projectRootFile}")" > /dev/null
                    ${script}
                    popd > /dev/null
                  '';

                  configurations =
                    if pkgs.hostPlatform.isDarwin then "darwinConfigurations" else "nixosConfigurations";

                  builder = if pkgs.hostPlatform.isDarwin then "darwin-rebuild" else lib.getExe pkgs.nixos-rebuild;
                in
                {
                  # TODO: allow running individual tests
                  test = {
                    description = "Run flake checks";
                    exec = runScriptInProjectRoot ''${lib.getExe pkgs.nix} flake check "$@"'';
                  };
                  run = {
                    description = "Run an app or package";
                    exec = runScriptInProjectRoot ''${lib.getExe pkgs.nix} run "$@"'';
                  };
                  build = {
                    description = "Build a package, check, or configuration";
                    exec = runScriptInProjectRoot ''${lib.getExe pkgs.nix} build "$@"'';
                  };
                  repl = {
                    description = "Open a REPL with flake inputs and outputs in scope";
                    exec = runScriptInProjectRoot "${lib.getExe pkgs.nix} repl .";
                  };
                  check-updates = {
                    description = "Report changes that will result from a rebuild";
                    exec = runScriptInProjectRoot ''${lib.getExe pkgs.nix} store diff-closures /run/current-system ".#${configurations}.$(hostname).config.system.build.toplevel"'';
                  };
                  update = {
                    description = "Update all flake inputs";
                    exec = runScriptInProjectRoot ''${lib.getExe pkgs.nix} flake update "$@"'';
                  };
                  update-input = {
                    description = "Update a single flake input";
                    exec = runScriptInProjectRoot ''${lib.getExe pkgs.nix} flake lock --update-input "$@"'';
                  };
                  update-package = {
                    description = "Update the source of a package with nix-update";
                    exec = runScriptInProjectRoot ''${lib.getExe pkgs.nix-update} "$1" -f ./packages.nix'';
                  };
                  edit = {
                    description = "Edit a file in pan9";
                    exec = runScriptInProjectRoot ''
                      "$EDITOR" "$1"
                    '';
                  };
                  edit-secret = {
                    description = "Edit a file in pan9-secrets with sops";
                    exec = runScriptInProjectRoot ''
                      ${lib.getExe pkgs.sops} "$(dirname "${config.flake-root.projectRootFile}")/../pan9-secrets/$1"
                    '';
                  };
                  rebuild = {
                    description = "Wrapper around nixos-rebuild/darwin-rebuild";
                    exec = runScriptInProjectRoot ''sudo -E ${builder} --flake . "''${1:-switch}" "''${@:1}"'';
                  };
                  deploy = {
                    description = "Run deploy-rs in the project root";
                    exec = runScriptInProjectRoot ''${pkgs.deploy-rs}/bin/deploy "$@"'';
                  };
                  git = {
                    description = "Run git in the project root";
                    exec = runScriptInProjectRoot ''${lib.getExe pkgs.git} "$@"'';
                  };
                  exec = {
                    description = "Run arbitrary shell commands";
                    exec = runScriptInProjectRoot ''"$@"'';
                  };
                };
            };

            checks = pkgs.lib.recursiveUpdate {
              nix =
                let
                  results = lib.concatLists (
                    map (
                      path:
                      import path {
                        inherit lib;
                        pkgs = pkgs';
                      }
                    ) [ ./tests/lib ]
                  );
                  resultToString =
                    {
                      name,
                      expected,
                      result,
                    }:
                    ''
                      ${name} failed: expected ${builtins.toJSON expected}, but got ${builtins.toJSON result}
                    '';
                in
                if results != [ ] then
                  builtins.throw (lib.concatStringsSep "\n" (map resultToString results))
                else
                  pkgs.runCommand "all-tests-passed" { } ''
                    touch $out
                  '';
            } packages;
          };
      })
      {
        nixosModules = lib.recursiveUpdate modules.nixos {
          inherit (modules) profiles;
          config = modules.config.nixos;
          default =
            { lib, ... }:
            {
              imports = lib.concatLists [
                [ modules.default ]
                (lib.collect builtins.isFunction modules.nixos)
                (lib.collect builtins.isFunction modules.profiles)
              ];
            };
        };

        darwinModules = {
          inherit (modules) profiles;
          default =
            { lib, ... }:
            {
              imports = lib.concatLists [
                [
                  modules.default
                  modules.nixos.users
                ]
                (lib.collect builtins.isFunction modules.profiles)
              ];
            };
        };

        hmModules = lib.recursiveUpdate modules.home-manager {
          config = modules.config.home-manager;
          default =
            { lib, ... }:
            {
              imports = lib.collect builtins.isFunction modules.home-manager;
            };
        };
      };
}
