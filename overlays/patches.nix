# Overlay patching upstream packages.
_final: prev: {
  shntool = prev.shntool.overrideAttrs (_oldAttrs: rec {
    patches = [ ./patches/shntool.patch ];
  });
}
