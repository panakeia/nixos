{ config, lib, ... }:

final: prev:

let
  inherit (lib) optionals concatLists;
  inherit (prev.stdenv.hostPlatform) isLinux isDarwin;
  inherit (final.config) allowUnfree;
in
{
  metaPackages = with prev; {
    cli = {
      core =
        [
          comma
          curl
          dnsutils
          fd
          file
          git
          htop
          jq
          magic-wormhole
          # use unwrapped neovim for smaller closure size
          neovim-unwrapped
          procps
          ripgrep
          tmux
          unzip
          watchexec
          wget
          zip
        ]
        ++ optionals isDarwin [ pinentry_mac ]
        ++ optionals isLinux [
          pinentry
          psmisc
        ];

      archive = [
        pigz
        pixz
      ] ++ optionals allowUnfree [ unrar ];

      binary = [
        binutils
        glasgow
        hexyl
      ] ++ optionals isLinux [ rizin ];

      creative = [ pastel ] ++ optionals isLinux [ orca-c ];

      desktop = optionals isLinux [
        alsa-utils
        libnotify
        protonmail-bridge
        ripgrep-all
        steam-run
        trash-cli
        wineWowPackages.full
        wineasio
        winetricks
        xdg-utils
        xdg-user-dirs
      ];

      hardware = optionals isLinux [
        ddcutil
        dmidecode
        libimobiledevice
        libmtp
        light
        pciutils
        usbutils
      ];

      media =
        [
          ffmpeg-full
          flac
          id3v2
          lame
          streamlink
          yt-dlp
        ]
        ++ optionals isLinux [
          shntool
          ttaenc
        ];

      misc = [
        eva
        units
      ];

      net = [
        aria2
        browsh
        socat
        nmap
        rclone
        wireguard-tools
      ];

      nix = [ nix-tree ];

      security = [
        age
        diceware
        openssl
        pass
        rage
      ];

      all = concatLists (
        with final.metaPackages.cli;
        [
          core
          archive
          binary
          creative
          desktop
          hardware
          media
          misc
          net
          nix
          security
        ]
      );
    };

    gui = {
      hardware = optionals isLinux [
        android-file-transfer
        piper
        yubioath-flutter
      ];

      net = optionals isLinux [
        input-leap
        ladybird
        networkmanagerapplet
        tor-browser
        transmission_3-gtk
        wireshark
      ];

      core = concatLists (
        with final.metaPackages.gui;
        [
          hardware
          net
          (optionals isLinux [
            anki
            foliate
            eog
            seahorse
            gnome-frog
            libreoffice
            libsForQt5.ark
            libsForQt5.kdeconnect-kde
            libsForQt5.marble
            patchage
            pavucontrol
            nixpkgs-unstable.valent
            virt-manager
            zathura
          ])
        ]
      );

      binary = optionals isLinux [
        # cutter
      ];

      creative =
        [ musescore ]
        ++ optionals isLinux [
          audacity
          blender
          gimp-with-plugins
          guitarix
          inkscape
          kdenlive
          krita
          puredata
          supercollider
          haskellPackages.tidal
          yabridgectl
        ]
        ++ optionals (allowUnfree && isLinux) [ bitwig-studio ];

      games = optionals isLinux [
        (lowPrio dolphin-emu)
        gnugo
        higan
        mednafen
        mednafen-server
        prismlauncher
        pcsx2
        piper
        proton-caller
        protontricks
        rpcs3
        ryujinx
        sabaki
        # unnamed-sdvx-clone
      ];

      media =
        [ quodlibet ]
        ++ optionals isLinux [
          aegisub
          ahoviewer
          handbrake
          haruna
          songrec
          soulseekqt
          syncplay
        ];

      social =
        [ element-desktop ]
        ++ optionals isLinux [
          mumble
          signal-desktop
          tdesktop
        ]
        ++ optionals (allowUnfree && isLinux) [ discord ];

      all = concatLists (
        with final.metaPackages.gui;
        [
          core
          binary
          creative
          games
          media
          social
        ]
      );
    };
  };
}
