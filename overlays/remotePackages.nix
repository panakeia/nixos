# Overlay providing packages from flakes and other remote dependencies.
{ inputs, nixpkgsConfig, ... }:

_final: prev:
let
  system = prev.stdenv.system;
in
{
  nixpkgs-unstable = import inputs.nixpkgs-unstable {
    inherit system;
    config = nixpkgsConfig;
  };
  nixpkgs-stable = import inputs.nixpkgs-stable {
    inherit system;
    config = nixpkgsConfig;
  };

  # NOTE: Needed for npmlock2nix, see https://github.com/nix-community/npmlock2nix/issues/194
  nodejs-16_x = prev.nodePkgs.nodejs;
  npmlock2nix = (import inputs.npmlock2nix { pkgs = prev; }).v2;
}
