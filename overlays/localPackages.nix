# Overlay providing locally defined packages from the ./packages directory.
final: prev:

let
  localPackagePaths = builtins.readDir ../packages;
  localPackages = (
    prev.lib.mapAttrs (name: _: (final.callPackage (../packages + "/${name}") { })) localPackagePaths
  );
in
localPackages // { inherit localPackages; }
