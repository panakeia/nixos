{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  cfg = config.programs.oil;
in
{
  options = {
    programs.oil = {
      enable = mkEnableOption "Whether to enable oil shell (osh)";
      oilrcExtra = mkOption {
        type = types.str;
        default = "";
        description = ''
          Configuration to add to "$XDG_CONFIG_HOME/oil/oilrc"
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [ oil ];

    xdg.configFile."oil/oilrc".text = ''
      # load .profile
      . "${config.home.file.".profile".source}"

      # load .bashrc
      . "${config.home.file.".bashrc".source}"

      ${cfg.oilrcExtra}
    '';
  };
}
