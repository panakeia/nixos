{
  config,
  lib,
  pkgs,
  inputs,
  nixpkgsConfig,
  ...
}@args:

with lib;

let
  inherit (lib.custom) mkLinux mkDarwin;
  shells = [
    pkgs.bashInteractive
    pkgs.zsh
  ];
in
{
  nix = mkMerge [
    {
      extraOptions = ''
        experimental-features = nix-command flakes
      '';

      settings = {
        # Give users in the "wheel" group additional privileges when connecting
        # to the Nix daemon.
        trusted-users = [
          "root"
          "@wheel"
        ];

        substituters = [ "https://nix-community.cachix.org" ];
        trusted-public-keys = [
          "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        ];
      };

      # Automatically garbage collect packages older than 30 days.
      gc = {
        automatic = true;
        options = "--delete-older-than 30d";
      };

      registry.self.flake = inputs.self;
    }

    (mkLinux {
      # Minimize the size of the Nix store.
      settings.auto-optimise-store = mkDefault true;

      # Run garbage collection service weekly (default is "daily")
      gc.dates = "weekly";
    })

    (mkDarwin {
      # Run garbage collection as the Nix store owner.
      gc.user = "pan";
    })
  ];

  nixpkgs = {
    config = nixpkgsConfig;
    overlays = [
      inputs.nur.overlay
      inputs."pan.ax".overlays.default
      inputs.self.overlays.default
      (import ../overlays/metaPackages.nix args)
    ];
  };

  home-manager = {
    # Make system configuration available from home-manager modules
    # via the `systemConfig` argument.
    extraSpecialArgs.systemConfig = config;
    extraSpecialArgs.inputs = inputs;
    extraSpecialArgs.lib = lib;

    # Import home-manager modules to all user profiles.
    sharedModules = [
      (import "${inputs.impermanence}/home-manager.nix")
      inputs.nix-index-database.hmModules.nix-index
      inputs.self.hmModules.default
      (lib.custom.mkLinux (
        lib.mkIf config.services.dictd.enable {
          home.file = {
            ".dictrc".text = ''
              server localhost
            '';
          };
        }
      ))
    ];
  };

  custom.users.pan = {
    enableSuperuser = true;
    email = "panakeia@protonmail.com";
    gpgEmail = "panaceaaaaaa@gmail.com";
    extraUserSettings = lib.mkMerge [
      { shell = pkgs.bashInteractive; }
      (lib.custom.mkLinux {
        openssh.authorizedKeys.keys = map builtins.readFile [
          "${inputs.self}/hosts/arcadia/ssh-public-keys/pan.pub"
          "${inputs.self}/hosts/argyre/ssh-public-keys/pan.pub"
        ];
      })
    ];
  };

  # Install default global packages.
  environment.systemPackages = pkgs.metaPackages.cli.core;
}
// (lib.custom.mkLinux {
  # Report version changes on system activation.
  system.activationScripts = {
    diff = ''
      ${config.nix.package}/bin/nix store --experimental-features 'nix-command' diff-closures /run/current-system "$systemConfig"
    '';
  };

  environment.shells = shells ++ [ "${pkgs.oils-for-unix}/bin/osh" ];

  # Use sops file from pan9-secrets input
  sops.defaultSopsFile = "${inputs.pan9-secrets}/${config.networking.hostName}/default.yaml";
})
// (lib.custom.mkDarwin {
  environment.shells = shells;

  services.nix-daemon.enable = mkDefault true;

  # Create /etc/bashrc that loads the nix-darwin environment.
  programs.bash.enable = mkDefault true;
  programs.zsh.enable = mkDefault true;
})
