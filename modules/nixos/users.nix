{
  config,
  lib,
  pkgs,
  inputs,
  ...
}@args:

let
  cfg = config.custom.users;
in
{
  options = {
    custom.users = lib.mkOption {
      type = lib.types.attrsOf (
        lib.types.submodule (
          { config, name, ... }:
          {
            options = {
              enableHome = lib.mkEnableOption "Whether to enable standard home-manager configuration for the user";
              enableSuperuser = lib.mkEnableOption "Whether the user should have superuser privileges";
              extraUserSettings = lib.mkOption {
                type = lib.types.attrs;
                default = { };
                description = "Additional settings to pass through to config.users.users.<name>";
              };
              extraHomeManagerSettings = lib.mkOption {
                type = lib.types.attrs;
                default = { };
                description = "Additional settings to pass through to home-manager.users.<name>";
              };
              username = lib.mkOption {
                type = lib.types.str;
                default = name;
              };
              fullName = lib.mkOption {
                type = lib.types.str;
                default = name;
              };
              email = lib.mkOption { type = lib.types.nullOr lib.types.str; };
              gpgEmail = lib.mkOption {
                type = lib.types.nullOr lib.types.str;
                default = config.email;
              };
            };
          }
        )
      );
    };
  };

  config = lib.custom.mkMergeIf (cfg != { }) [
    {
      users.users = lib.mapAttrs (
        _name: userCfg:
        lib.mkMerge [
          {
            home =
              if pkgs.stdenv.hostPlatform.isDarwin then
                "/Users/${userCfg.username}"
              else
                "/home/${userCfg.username}";
          }

          (lib.custom.mkLinux {
            isNormalUser = lib.mkDefault true;
            extraGroups = lib.optionals userCfg.enableSuperuser (
              [
                "wheel"
                "systemd-journal"
              ]
              ++ lib.optionals config.custom.profiles.desktop.enable [
                "audio"
                "video"
                "lp"
              ]
              ++ lib.optionals config.networking.networkmanager.enable [ "networkmanager" ]
              ++ lib.optionals config.services.transmission.enable [ "transmission" ]
              ++ lib.optionals config.virtualisation.libvirtd.enable [ "libvirtd" ]
              ++ lib.optionals config.services.usbmuxd.enable [ "fuse" ]
            );
          })

          userCfg.extraUserSettings
        ]
      ) cfg;

      home-manager.users = lib.mapAttrs (
        _name: userCfg:
        let
          inherit (inputs.self.hmModules.config) shell x11 wayland;
          isWayland = pkgs.stdenv.hostPlatform.isLinux && config.programs.xwayland.enable;

          configModules =
            [ shell ]
            ++ lib.optionals config.custom.profiles.desktop.enable (
              lib.attrValues {
                inherit (inputs.self.hmModules.config)
                  browser
                  communication
                  desktop
                  editor
                  media
                  term
                  ;
              }
            )
            ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
              if config.custom.profiles.desktop.enable then (if isWayland then [ wayland ] else [ x11 ]) else [ ]
            );

          moduleArgs = lib.recursiveUpdate args {
            systemConfig = config;
            userConfig = config.users.users.${userCfg.username};
            customUserConfig = userCfg;
            config = config.home-manager.users.${userCfg.username};
            lib = lib.recursiveUpdate lib {
              custom = lib.recursiveUpdate lib.custom {
                # (pathToUserDir "download") == "/home/user/Downloads"
                pathToUserDir =
                  dirName:
                  let
                    userDir = config.home-manager.users.${userCfg.username}.xdg.userDirs.${dirName};
                  in
                  lib.replaceStrings [ "$HOME" ] [ config.users.users.${userCfg.username}.home ] userDir;
              };
            };
          };
        in
        lib.mkMerge (
          (map (module: module moduleArgs) configModules) ++ [ userCfg.extraHomeManagerSettings ]
        )
      ) cfg;
    }

    (lib.custom.mkDarwin {
      users.groups.wheel.members = lib.mapAttrsToList (username: _: username) (
        lib.filterAttrs (_: userCfg: userCfg.isSuperuser) cfg
      );
    })
  ];
}
