{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  cfg = config.custom.services.ashuffle;
in
{
  options = {
    custom.services.ashuffle = {
      enable = mkEnableOption "Whether to enable ashuffle, an automatic shuffler for mpd";
      host = mkOption {
        type = types.str;
        default = "127.0.0.1";
        example = "mpd.example.com";
        description = ''
          The address at which to access the mpd server.
        '';
      };
      port = mkOption {
        type = types.int;
        default = 6600;
        description = ''
          The TCP port which the mpd server is listening on.
        '';
      };
      queueBuffer = mkOption {
        type = types.int;
        default = 1;
        description = ''
          How many tracks to buffer in the queue.
          Should be greater than 1 e.g. if crossfade between tracks is desired.
        '';
      };
      groupBy = mkOption {
        type = types.nullOr types.str;
        default = null;
        example = "album date";
        description = ''
          List of tags to group shuffled songs by.
          Songs in a group will all be added to the queue.
          Can be used to queue an album's worth of songs, instead of one at a time.
        '';
      };
      user = mkOption {
        type = types.str;
        default = "ashuffle";
        description = "User account under which ashuffle runs.";
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.ashuffle = {
      description = "Automatic shuffling for mpd";
      after = [ "network.target" ] ++ lib.optionals (config.services.mpd.enable) [ "mpd.service" ];
      wantedBy = [ "multi-user.target" ];

      serviceConfig = {
        User = cfg.user;
        ExecStart = "${pkgs.ashuffle}/bin/ashuffle --host ${cfg.host} --port ${toString cfg.port} --queue-buffer ${toString cfg.queueBuffer} ${
          lib.optionalString (cfg.groupBy != null) "--group-by ${cfg.groupBy}"
        }";
        Restart = "always";
        DynamicUser = "yes";
      };
    };
  };
}
