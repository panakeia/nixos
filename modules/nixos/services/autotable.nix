{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  cfg = config.custom.services.autotable;

  # Hardcoded in server/index.ts
  serverPort = 1235;
in
{
  options = {
    custom.services.autotable = {
      enable = mkEnableOption "Whether to enable autotable, an online mahjong table.";
      user = mkOption {
        type = types.str;
        default = "autotable";
        description = "The user to run the websocket server as.";
      };
      frontend = {
        enable = mkOption {
          type = types.bool;
          default = true;
          description = "Whether to serve the frontend with nginx.";
        };
        listenAddr = mkOption {
          type = types.str;
          default = "*";
          example = "0.0.0.0";
          description = "IP to listen on.";
        };
        listenPort = mkOption {
          type = types.int;
          default = 80;
          example = 443;
          description = "Port to listen on.";
        };
      };
    };
  };

  config = mkIf cfg.enable {
    services.nginx = {
      enable = cfg.frontend.enable;
      virtualHosts.autotable = {
        listen = [
          {
            addr = cfg.frontend.listenAddr;
            port = cfg.frontend.listenPort;
          }
        ];
        # Recommended configuration from upstream, see
        # https://github.com/pwmarcz/autotable#deployment
        locations = {
          "/autotable/".extraConfig = ''
            expires 0d;
            alias ${pkgs.autotable}/;
          '';
          "/autotable/ws" = {
            proxyPass = "http://127.0.0.1:${builtins.toString serverPort}/";
            extraConfig = ''
              proxy_http_version 1.1;
              proxy_set_header Upgrade $http_upgrade;
              proxy_set_header Connection "upgrade";
              proxy_read_timeout 7d;
            '';
          };
        };
      };
    };

    systemd.services.autotable = {
      description = "An online mahjong table";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];

      serviceConfig = {
        User = cfg.user;
        ExecStart = "${pkgs.nodejs}/bin/node ./index.js";
        WorkingDirectory = "${pkgs.autotableServer}";
        Restart = "always";
        DynamicUser = "yes";
      };
    };
  };
}
