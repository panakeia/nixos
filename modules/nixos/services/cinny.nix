{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  cfg = config.custom.services.cinny;
in
{
  options = {
    custom.services.cinny = {
      enable = mkEnableOption "Whether to enable cinny.";
      listenAddr = mkOption {
        type = types.str;
        default = "*";
        example = "0.0.0.0";
        description = "IP to listen on.";
      };
      listenPort = mkOption {
        type = types.int;
        default = 80;
        example = 443;
        description = "Port to listen on.";
      };
    };
  };

  config = mkIf cfg.enable {
    services.nginx = {
      enable = cfg.enable;
      virtualHosts.cinny = {
        listen = [
          {
            addr = cfg.listenAddr;
            port = cfg.listenPort;
          }
        ];
        # Recommended configuration from upstream, see
        # https://github.com/ajbura/cinny/tree/master/contrib/nginx
        locations = {
          "/".extraConfig = ''
            root ${pkgs.cinny}/;
          '';
          "~* ^/(login|register)".extraConfig = ''
            try_files $uri $uri/ /index.html;
          '';
        };
      };
    };
  };
}
