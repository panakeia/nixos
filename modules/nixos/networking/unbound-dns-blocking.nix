{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

let
  cfg = config.custom.networking.unbound-dns-blocking;
in
{
  options = {
    custom.networking.unbound-dns-blocking = {
      enable = lib.mkEnableOption ''
        Whether to use unbound to block certain hosts.
      '';
      blockOnCalendar = lib.mkOption {
        type = lib.types.str;
        default = "Mon..Fri, 17:00";
        description = ''
          An OnCalendar expression which describes when temporary DNS blocking
          should begin to take effect.
        '';
      };
      unblockOnCalendar = lib.mkOption {
        type = lib.types.str;
        default = "Mon..Fri, 06:00";
        description = ''
          An OnCalendar expression which describes when temporary DNS blocking
          should cease to take effect.
        '';
      };
      temporaryHosts = lib.mkOption {
        type = lib.types.listOf lib.types.str;
        default = [ ];
        description = ''
          The list of hosts which should be blocked according to the schedule
          defined in blockOnCalendar/unblockOnCalendar.
        '';
      };
    };
  };

  config = lib.mkIf cfg.enable {
    # Include all unbound configuration files not prefixed by "exclude" in
    # /etc/unbound/include.
    services.unbound.settings.server.include = "/etc/unbound/include/[!exclude]*.conf";

    # These hosts (from StevenBlack/hosts) are always blocked.
    environment.etc."unbound/include/blocking.conf".source =
      pkgs.runCommand "write-unbound-blocking" { }
        ''
          touch $out
          ${pkgs.gawk}/bin/awk '/^# Start StevenBlack/,0' ${inputs.hosts}/hosts | awk '$1 == "0.0.0.0" {print "local-zone: \""$2"\" always_nxdomain"}' > $out
        '';

    # These hosts are user-defined and are only blocked on a temporary
    # basis.
    environment.etc."unbound/include/exclude-temporary-blocking.conf".text = lib.concatStringsSep "\n" (
      map (host: ''local-zone: "${host}" always_nxdomain'') cfg.temporaryHosts
    );

    systemd.timers = {
      start-temporary-blocking = {
        wantedBy = [ "timers.target" ];
        timerConfig.OnCalendar = cfg.blockOnCalendar;
      };

      stop-temporary-blocking = {
        wantedBy = [ "timers.target" ];
        timerConfig.OnCalendar = cfg.unblockOnCalendar;
      };
    };

    systemd.services = {
      start-temporary-blocking = {
        description = "Block DNS resolution of hosts";
        serviceConfig = {
          ExecStart = "${pkgs.coreutils}/bin/ln -s /etc/unbound/include/exclude-temporary-blocking.conf /etc/unbound/include/temporary-blocking.conf";
          ExecStartPost = "systemctl try-restart unbound.service";
        };
      };

      stop-temporary-blocking = {
        description = "Unblock DNS resolution of hosts";
        serviceConfig = {
          ExecStart = "${pkgs.coreutils}/bin/rm /etc/unbound/include/temporary-blocking.conf";
          ExecStartPost = "systemctl try-restart unbound.service";
        };
      };
    };
  };
}
