{
  config,
  lib,
  inputs,
  ...
}:

with lib;

let
  cfg = config.custom.networking.wireguard-site-to-site;
  inherit (inputs.self.wireguardNetworks."${cfg.networkName}".peers)
    clients
    servers
    ;
  allPeers = servers // clients;
  selfPeer = allPeers.${config.networking.hostName};
in
{
  options = {
    custom.networking.wireguard-site-to-site = {
      enable = mkEnableOption "";
      enableServer = mkEnableOption "";
      domain = mkOption { };
      networkName = mkOption { };
      setPrivateKeyFile = mkOption { default = true; };
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      networking = {
        wireguard.enable = true;
        wg-quick.interfaces.${cfg.networkName}.privateKeyFile = mkIf cfg.setPrivateKeyFile "/persist/etc/wireguard/${cfg.networkName}";
      };

      programs.ssh.knownHosts = mkIf config.services.openssh.enable (
        mapAttrs (name: value: {
          hostNames = [
            "${name}.${cfg.domain}"
            "${value.ip}"
          ] ++ (inputs.self.sshHostKeys.${name}.aliases or [ ]);
          publicKeyFile =
            if (hasAttr name inputs.self.sshHostKeys) then
              "${inputs.self.sshHostKeys."${name}".publicKeyFile}"
            else
              null;
        }) (filterAttrs (name: _: hasAttr name inputs.self.sshHostKeys) allPeers)
      );

      services.openssh.listenAddresses = [
        {
          addr = selfPeer.ip;
          port = 22;
        }
      ];

      # NOTE: make sure sshd does not attempt to start before wireguard
      # interface is up
      #
      # https://github.com/NixOS/nixpkgs/issues/105570
      systemd.services.sshd.requires = [ "wg-quick-${cfg.networkName}.service" ];
      systemd.services.sshd.after = [ "wg-quick-${cfg.networkName}.service" ];
    })

    (mkIf (cfg.enable && cfg.enableServer) {
      boot.kernel.sysctl = {
        "net.ipv4.ip_forward" = mkDefault true;
        "net.ipv6.conf.all.forwarding" = mkDefault true;
      };

      networking = {
        extraHosts = mkIf cfg.enableServer (
          builtins.concatStringsSep "\n" (
            mapAttrsToList (name: value: "${value.ip} ${name}.${cfg.domain}") allPeers
          )
        );
        wg-quick.interfaces.${cfg.networkName} = {
          address = [ "${selfPeer.ip}/16" ];
          listenPort = 51820;
          mtu = 1412;
          peers = map (client: {
            publicKey = client.publicKey;
            allowedIPs = [ "${client.ip}/32" ];
          }) (attrValues clients);
        };
      };
    })

    (mkIf (cfg.enable && !cfg.enableServer) {
      networking.wg-quick.interfaces.${cfg.networkName} = {
        address = [ "${selfPeer.ip}/32" ];
        # Forward DNS to all servers, and add the networkName as a search domain.
        dns = (map (server: server.ip) (attrValues servers)) ++ [ cfg.domain ];
        peers = map (getAttrs [
          "publicKey"
          "endpoint"
          "allowedIPs"
          "persistentKeepalive"
        ]) (attrValues servers);
      };
    })
  ];
}
