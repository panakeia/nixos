{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  inherit (lib.custom) mkLinux mkMergeIf;
  cfg = config.custom.profiles.gaming;
in
{
  options = {
    custom.profiles.gaming = {
      enable = mkEnableOption "Enable profile which provides support for games.";
    };
  };

  config = mkMergeIf cfg.enable (
    singleton (mkLinux {
      boot.extraModulePackages = [ config.boot.kernelPackages.gcadapter-oc-kmod ];

      programs.corectrl.enable = true;

      programs.steam.enable = mkDefault config.nixpkgs.config.allowUnfree;

      hardware.steam-hardware.enable = true;

      services.udev.packages = [ pkgs.dolphin-emu ];
    })
  );
}
