{ lib, ... }@args:
lib.custom.mkProfile args {
  name = "kde";
  description = "Whether to enable KDE";
  default = false;
  settings = lib.custom.mkLinux {
    custom.profiles.desktop.enable = true;

    services.libinput.enable = true;

    services.displayManager = {
      sddm.enable = lib.mkDefault true;
      sddm.wayland.enable = lib.mkDefault true;
      defaultSession = "plasma";
    };

    services.desktopManager.plasma6.enable = true;
  };
}
