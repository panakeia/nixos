{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  inherit (lib.custom) mkLinux mkMergeIf;
  cfg = config.custom.profiles.gnome;
in
{
  options = {
    custom.profiles.gnome = {
      enable = mkEnableOption "Enable GNOME profile.";
    };
  };

  config = mkMergeIf cfg.enable (
    singleton (mkLinux {
      custom.profiles.desktop.enable = true;

      environment.systemPackages = with pkgs; [
        gnome.dconf-editor
        gnome.file-roller
        gnome.gnome-screenshot
        gnomeExtensions.caffeine
        gnomeExtensions.paperwm
        gnomeExtensions.gsconnect
        gnomeExtensions.pip-on-top
      ];

      environment.gnome.excludePackages = with pkgs; [ gnome.gnome-initial-setup ];

      services.xserver = {
        enable = true;
        libinput.enable = true;

        desktopManager.gnome = {
          enable = true;
          flashback.customSessions = [
            {
              wmName = "sway";
              wmLabel = "Sway";
              wmCommand = "${pkgs.sway}/bin/sway";
            }
          ];
        };

        displayManager.gdm = {
          # Allow other display managers to be used
          enable = mkDefault true;
        };
      };

      # Disable unwanted core/shell services
      services.avahi.enable = mkForce false;
      services.telepathy.enable = mkForce false;

      services.gnome = {
        core-utilities.enable = mkForce false;
        evolution-data-server.enable = mkForce false;
        gnome-online-miners.enable = mkForce false;
        gnome-remote-desktop.enable = mkForce false;
        gnome-user-share.enable = mkForce false;
        rygel.enable = mkForce false;
        tracker.enable = mkForce false;
        tracker-miners.enable = mkForce false;
        gnome-keyring.enable = mkForce false;
        gnome-initial-setup.enable = mkForce false;
        gnome-browser-connector.enable = mkForce false;
      };

      programs.gnome.gnome-files.enable = mkDefault true;
    })
  );
}
