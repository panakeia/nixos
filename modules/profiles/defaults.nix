{
  config,
  lib,
  pkgs,
  ...
}@args:
lib.custom.mkProfile args {
  name = "defaults";
  description = ''
    Whether to enable default settings which diverge from upstream modules.
    Settings in this module are generally recommended by upstream or otherwise
    uncontroversial, but not implemented e.g. for compatibility reasons.
  '';
  default = true;
  settings = lib.mkMerge [
    {
      home-manager = {
        # Write home-manager profile to /etc/profiles/per-user/$USER instead of
        # /home/$USER.
        useUserPackages = true;

        # Reuse the pkgs argument from the system configuration inside
        # home-manager modules.
        useGlobalPkgs = true;

        sharedModules = [
          # "Once tested and deemed sufficiently robust, this will become the default."
          # https://github.com/nix-community/home-manager/blob/master/modules/systemd.nix#L174
          {
            systemd.user.startServices = "sd-switch";
          }

          # Allow users to install unfree packages if their system is configured to do so.
          (
            { systemConfig, ... }:
            lib.mkIf systemConfig.nixpkgs.config.allowUnfree {
              xdg.configFile."nixpkgs/config.nix".text = ''
                { allowUnfree = true; }
              '';
            }
          )
        ];
      };
    }

    (lib.custom.mkLinux {
      # Save space by disabling documentation and unused locales.
      documentation.enable = lib.mkDefault false;
      i18n.supportedLocales = [ "en_US.UTF-8/UTF-8" ];

      boot = {
        # Use the most recent kernel.
        kernelPackages = pkgs.linuxPackages_latest;

        # "It is recommended to set this to false, as it allows gaining root
        # access by passing init=/bin/sh as a kernel parameter."
        # https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/system/boot/loader/systemd-boot/systemd-boot.nix#L59
        loader.systemd-boot.editor = false;

        # Mount a tmpfs on /tmp and wipe the contents during boot. The first
        # option implies the second, but if `useTmpfs` is disabled (e.g. for
        # a system which cannot spare the RAM) /tmp should still be wiped at
        # boot.
        tmp = {
          useTmpfs = true;
          cleanOnBoot = true;
        };
      };

      # "Using this option is highly discouraged and also incompatible with
      # networking.useNetworkd", therefore we set it to false.
      # https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/tasks/network-interfaces.nix#L1145
      networking.useDHCP = false;

      users.mutableUsers = false;

      # TODO: use usersWithPasswords here if possible
      # usersWithPasswords =
      #   lib.filterAttrs (name: cfg: name == "root" || cfg.isNormalUser)
      #   config.users.users;
      users.users.root.hashedPasswordFile = config.sops.secrets.root_password.path;
      users.users.pan.hashedPasswordFile = config.sops.secrets.pan_password.path;
      sops.secrets.root_password.neededForUsers = true;
      sops.secrets.pan_password.neededForUsers = true;

      # TODO: remove if https://github.com/systemd/systemd/issues/6238 is ever fixed
      services.journald.extraConfig = ''
        SystemMaxUse=512M
        RuntimeMaxUse=512M
        MaxRetentionSec=1week
      '';

      services.openssh = {
        # Most NixOS services do not open ports in the firewall automatically
        # when enabled, SSH being the exception "for historical reasons".
        # https://github.com/NixOS/nixpkgs/issues/19504#issuecomment-253458728
        # https://github.com/NixOS/nixpkgs/pull/81490
        openFirewall = false;

        # Basic hardening settings - authentication via keys only, no login as root
        settings = {
          PermitRootLogin = "no";
          PasswordAuthentication = false;
          KbdInteractiveAuthentication = false;
        };
      };
    })
  ];
}
