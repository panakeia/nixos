{ config, lib, ... }@args:
lib.custom.mkProfile args {
  name = "audio";
  description = "Whether to support audio";
  default = config.custom.profiles.desktop.enable;
  settings = lib.custom.mkLinux {
    # Make programs support pulseaudio if possible.
    nixpkgs.config.pulseaudio = true;

    # Disable pulseaudio.
    hardware.pulseaudio.enable = lib.mkForce false;

    # Enable realtime scheduling.
    security.rtkit.enable = lib.mkDefault true;

    # Enable Pipewire with emulation for alsa, pulseaudio, and jack
    services.pipewire = {
      enable = lib.mkDefault true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
      jack.enable = true;
    };

    # Create Pipewire virtual sinks
    services.pipewire.extraConfig.pipewire = {
      "proxy" = {
        "context.objects" = [
          {
            factory = "adapter";
            args = {
              "factory.name" = "support.null-audio-sink";
              "node.name" = "Microphone-Proxy";
              "node.description" = "Microphone";
              "media.class" = "Audio/Source/Virtual";
              "audio.position" = "MONO";
            };
          }
          {
            factory = "adapter";
            args = {
              "factory.name" = "support.null-audio-sink";
              "node.name" = "Main-Output-Proxy";
              "node.description" = "Main Output";
              "media.class" = "Audio/Sink";
              "audio.position" = "FL,FR";
            };
          }
        ];
      };
    };
  };
}
