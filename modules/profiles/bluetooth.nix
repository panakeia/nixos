{
  config,
  lib,
  pkgs,
  ...
}@args:
lib.custom.mkProfile args {
  name = "bluetooth";
  description = "Whether to support Bluetooth devices";
  default = config.custom.profiles.desktop.enable;
  settings = lib.custom.mkLinux {
    hardware.bluetooth = {
      enable = lib.mkDefault true;
      package = pkgs.bluez;
    };
    services.blueman.enable = lib.mkDefault (
      !config.custom.profiles.gnome.enable && !config.custom.profiles.kde.enable
    );
  };
}
