{ config, lib, ... }@args:
lib.custom.mkProfile args {
  name = "zfs";
  description = "Whether to enable ZFS configuration";
  default = config.boot.zfs.enabled;
  settings = lib.custom.mkLinux {
    services.zfs = {
      # Automatically scrub zfs pools to identify problems.
      autoScrub = {
        enable = lib.mkDefault true;
        interval = "daily";
      };

      # Make automatic snapshots of datasets which have the com.sun:auto-snapshot
      # property set to true.
      autoSnapshot = {
        enable = lib.mkDefault true;
        flags = "-k -p --utc";
      };
    };
  };
}
