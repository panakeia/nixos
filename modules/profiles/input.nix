{
  config,
  lib,
  pkgs,
  ...
}@args:
lib.custom.mkProfile args {
  name = "input";
  description = "Whether to enable common configuration for input devices";
  default = config.custom.profiles.desktop.enable;
  settings = lib.mkMerge [
    (lib.custom.mkLinux {
      environment.systemPackages =
        with pkgs;
        lib.optionals config.services.ratbagd.enable [
          # GUI for interacting with `ratbagd`
          piper
        ];

      services = {
        # Allow configuring certain mice via `piper`/`ratbagctl`
        ratbagd.enable = lib.mkDefault true;

        # Interpret Caps Lock key and Control key as Control
        xserver.xkb.options = "ctrl:nocaps";
      };

      # Disable mouse acceleration
      services.libinput.touchpad.accelProfile = "flat";

      # Replicate `services.xserver.xkbOptions` in Linux virtual console (tty)
      console.useXkbConfig = lib.mkDefault true;
    })

    (lib.custom.mkDarwin {
      system = {
        defaults = {
          trackpad = {
            # Enable tap to click with the trackpad.
            Clicking = true;
          };

          NSGlobalDomain = {
            # Delay before keys are repeated and between key repeats, respectively.
            InitialKeyRepeat = 25;
            KeyRepeat = 2;
          };
        };

        keyboard = {
          enableKeyMapping = true;
          remapCapsLockToControl = true;
        };
      };
    })
  ];
}
