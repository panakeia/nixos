{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  inherit (lib.custom) mkLinux mkMergeIf;
  cfg = config.custom.profiles.minimalX11;
in
{
  options = {
    custom.profiles.minimalX11 = {
      enable = mkEnableOption ''
        Enable minimal X server profile, using lightdm for login and including
        shims for GNOME desktop applications. This profile does not provide
        a window manager - you are expected to use home-manager to configure
        one instead.
      '';
    };
  };

  config = mkMergeIf cfg.enable (
    lib.singleton (mkLinux {
      services.libinput.enable = true;

      services.xserver = {
        enable = true;
        displayManager.lightdm = {
          enable = true;
          background = "${pkgs.mkSolidColorWallpaper "#555476"}/wallpaper.png";
          greeters.gtk = {
            enable = true;
            theme = {
              package = pkgs.onestepback;
              name = "OneStepBack";
            };
            extraConfig = ''
              hide-user-image = true
            '';
          };
        };
      };
    })
  );
}
