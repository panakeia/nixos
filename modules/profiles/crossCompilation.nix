{ lib, ... }@args:
lib.custom.mkProfile args {
  name = "crossCompilation";
  description = "Whether to support cross-compilation to aarch64";
  default = false;
  settings = lib.custom.mkLinux {
    boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
    virtualisation.libvirtd.enable = true;
  };
}
