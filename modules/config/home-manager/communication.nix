{
  lib,
  userConfig,
  inputs,
  ...
}:

lib.mkMerge [
  {
    programs.irssi = {
      enable = lib.mkDefault true;
      networks = {
        revision = {
          nick = userConfig.name;
          server = {
            address = "irc.revision-party.net";
            port = 6697;
            autoConnect = true;
          };
          channels = {
            wrd.autoJoin = true;
            revision.autoJoin = true;
          };
        };
      };
    };

    xdg.configFile."irssi/default.theme".text = builtins.readFile "${inputs.self}/dotfiles/pan/irssi/default.theme";
  }

  (lib.custom.mkLinux {
    programs.thunderbird = {
      enable = true;
      profiles.home = {
        isDefault = true;
      };
    };

    services.pantalaimon = {
      enable = lib.mkDefault true;
      settings = {
        Default = {
          LogLevel = "Debug";
          SSL = true;
        };
        local-matrix = {
          Homeserver = "https://matrix.org";
          ListenAddress = "localhost";
          ListenPort = 8008;
        };
      };
    };
  })
]
