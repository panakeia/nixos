{
  config,
  lib,
  pkgs,
  systemConfig,
  ...
}:

{
  programs.autorandr.enable = lib.mkDefault true;

  programs.rofi = {
    enable = lib.mkDefault true;
    package = pkgs.rofi.override {
      plugins = with pkgs; [
        rofi-calc
        # rofi-emoji
        rofi-pass
        rofi-systemd
      ];
    };
    terminal = "${pkgs.alacritty}/bin/alacritty";
  };

  services.caffeine.enable = lib.mkDefault true;

  services.dunst = {
    enable = lib.mkDefault true;
    settings = {
      urgency_normal.background = "#000000";
    };
  };

  services.gammastep = {
    enable = lib.mkDefault true;
    provider = "geoclue2";
    settings.general.brightness-night = "0.75";
  };

  services.screen-locker = {
    enable = lib.mkDefault true;
    inactiveInterval = 5;
    lockCmd = "${pkgs.xsecurelock}/bin/xsecurelock";
  };

  services.sxhkd = {
    enable = lib.mkDefault true;
    keybindings = {
      # lock screen
      "super + ctrl + q" = "${pkgs.xorg.xset}/bin/xset s activate";
      # launcher
      "super + space" = ''${pkgs.rofi}/bin/rofi -modi "drun,run" -show drun'';
      # spawn terminal
      "super + Return" = "${pkgs.alacritty}/bin/alacritty";
      # spawn terminal (emacs)
      "super + shift + Return" = "${config.programs.emacs.package}/bin/emacsclient -c -a '' --eval '(+vterm/here nil))'";
      # screenshot monitor
      "super + ctrl + 3" = "${pkgs.maim}/bin/maim | ${pkgs.xclip}/bin/xclip -selection clipboard -t image/png";
      # screenshot selected region
      "super + ctrl + 4" = "${pkgs.maim}/bin/maim -s | ${pkgs.xclip}/bin/xclip -selection clipboard -t image/png";
      # quit/restart bspwm
      "super + alt + {q,r}" = "${pkgs.bspwm}/bin/bspc {quit,wm -r}";
      # toggle monocle
      "super + m" = "${pkgs.bspwm}/bin/bspc desktop -l next";
      # rotate windows
      "super + {_,shift} + r" = "${pkgs.bspwm}/bin/bspc node @/ -C {forward,backward}";
      # previous/next desktop (one monitor)
      "super + {_,shift} + bracket{left,right}" = "${pkgs.bspwm}/bin/bspc {desktop -f,node -d} {prev,next}";
      # previous/next desktop (all monitors)
      # "super + alt + bracket{left,right}" = ''
      #  cycle_dir={prev,next}; \
      #  ${pkgs.bspwm}/bin/bspc desktop "$\{cycle_dir\}.local" -f; \
      #  for mon_id in $(${pkgs.bspwm}/bin/bspc query -M -m '.!focused'); do \
      #    ${pkgs.bspwm}/bin/bspc desktop "$\{mon_id\}:focused#$\{cycle_dir\}.local" -a; \
      #  done
      #  '';
      # focus or send to the given desktop (one monitor)
      "super + {_,shift + }{1-9,0}" = ''${pkgs.bspwm}/bin/bspc {desktop -f,node -d} "^{1-9,10}"'';
      # focus the given desktop (all monitors)
      # "super + alt + {1-9,0}" = ''
      #  for MONITOR in $(${pkgs.bspwm}/bin/bspc query -M); do \
      #    ${pkgs.bspwm}/bin/bspc desktop -f "$(bspc query -D -m "$MONITOR" | sed "{1-9,10}q;d")"; \
      #  done
      # '';
      # close window
      "super + w" = "${pkgs.bspwm}/bin/bspc node -c";
      # kill
      "super + alt + esc" = "${pkgs.bspwm}/bin/bspc node -k";
      # set window state
      "super + ctrl + {t,shift+t,s,f}" = "${pkgs.bspwm}/bin/bspc node -t {tiled,pseudo_tiled,floating,fullscreen}";
      # set node flags
      "super + ctrl + {m,x,y,z}" = "${pkgs.bspwm}/bin/bspc node -g {marked,locked,sticky,private}";
      # focus the node in the given direction
      "super + {_,shift + }{h,j,k,l}" = "${pkgs.bspwm}/bin/bspc node -{f,s} {west,south,north,east}";
      # summon window switcher
      "super + {_,shift + }Tab" = ''${pkgs.rofi}/bin/rofi -show {windowcd,window} -kb-accept-entry "!Super-Tab,!Super_L,!Super+Super_L,Return" -kb-row-down "Super+Tab" -selected-row 2'';
      # focus the last node/desktop
      "super + {_,shift + }grave" = "${pkgs.bspwm}/bin/bspc {node,desktop} -f last";
      # expand a window by moving one of its sides outward
      "super + alt + {h,j,k,l}" = "${pkgs.bspwm}/bin/bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}";
      # contract a window by moving one of its sides inward
      "super + alt + shift + {h,j,k,l}" = "${pkgs.bspwm}/bin/bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}";
      # move a floating window
      "super + {Left,Down,Up,Right}" = "${pkgs.bspwm}/bin/bspc node -v {-20 0,0 20,0 -20,20 0}";
      "XF86Audio{Raise,Lower}Volume" = "${pkgs.pulsemixer}/bin/pulsemixer --change-volume {+,-}10";
      "XF86AudioMute" = "${pkgs.pulsemixer}/bin/pulsemixer --toggle-mute";
    };
  };

  xsession = {
    enable = lib.mkDefault true;
    scriptPath = ".xsession-hm";
    windowManager.bspwm = {
      enable = lib.mkDefault true;
      rules = {
        "Discord" = {
          state = "tiled";
        };
        "Emacs" = {
          state = "tiled";
        };
        "Next" = {
          state = "tiled";
        };
        "Steam" = {
          state = "floating";
        };
        "mpv" = {
          state = "floating";
        };
      };
      # FIXME: Both remove_disabled_monitors and remove_unplugged_monitors are set
      # to false to prevent a segfault in bspwm that happens on resume:
      #
      # https://github.com/baskerville/bspwm/issues/931
      settings = {
        click_to_focus = "button1";
        pointer_action1 = "move";
        pointer_action2 = "resize_corner";
        pointer_motion_interval = 7;
        remove_disabled_monitors = false;
        remove_unplugged_monitors = false;
        border_width = 1;
        window_gap = 2;
        focused_border_color = "#000000";
        active_border_color = "#000000";
        normal_border_color = "#000000";
        split_ratio = 0.5;
        borderless_monocle = true;
        gapless_monocle = true;
      };
      extraConfig = ''
        ${pkgs.autorandr}/bin/autorandr -l ${systemConfig.networking.hostName}

        ALL_MONITORS=$(${pkgs.xorg.xrandr}/bin/xrandr -q | grep '\bconnected\b')
        PRIMARY_MONITORS=$(${pkgs.coreutils}/bin/echo "$ALL_MONITORS" | ${pkgs.gnugrep}/bin/grep '\bprimary\b' | ${pkgs.coreutils}/bin/cut -f 1 -d " ")
        OTHER_ACTIVE_MONITORS=$(${pkgs.coreutils}/bin/echo "$ALL_MONITORS" | ${pkgs.gnugrep}/bin/grep -v '\bprimary\b' | ${pkgs.gnugrep}/bin/grep '[0-9]+' | ${pkgs.coreutils}/bin/cut -f 1 -d " ")

        for MONITOR in $PRIMARY_MONITORS; do
            ${pkgs.bspwm}/bin/bspc monitor "$MONITOR" -d I II III IV V
        done

        for MONITOR in $OTHER_CONNECTED_MONITORS; do
            ${pkgs.bspwm}/bin/bspc monitor "$MONITOR" -d I
        done
      '';
    };
  };
}
