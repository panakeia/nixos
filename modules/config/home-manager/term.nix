{ lib, ... }:

{
  programs.alacritty = {
    enable = lib.mkDefault true;
    settings = {
      window = {
        padding = {
          x = 8;
          y = 8;
        };
      };
      font = {
        size = 12.5;
      };
    };
  };
}
