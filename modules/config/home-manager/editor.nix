{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

let
  inherit (builtins) substring stringLength;
  inherit (lib) mkDefault mkMerge;
  inherit (lib.custom) mkLinux;

  defaultGpgKey = config.programs.gpg.settings.default-key;
in
mkMerge [
  {
    home.packages = [
      pkgs.ispell
      (pkgs.makeDesktopItem rec {
        name = "org-protocol";
        desktopName = "org-protocol";
        exec = "emacsclient %u";
        type = "Application";
        terminal = false;
        categories = [ "System" ];
        mimeTypes = [ "x-scheme-handler/org-protocol" ];
      })
    ];

    home.sessionVariables.EDITOR = "nvim";

    xdg.configFile = {
      "doom/config.el".text = builtins.readFile "${inputs.self}/dotfiles/pan/doom/config.el";
      "doom/init.el".text = builtins.readFile "${inputs.self}/dotfiles/pan/doom/init.el";
      "doom/packages.el".text = builtins.readFile "${inputs.self}/dotfiles/pan/doom/packages.el";
    };

    programs.emacs = {
      enable = mkDefault true;
      package = pkgs.emacs29;
      extraConfig =
        let
          email = substring 1 ((stringLength defaultGpgKey) - 1) defaultGpgKey;
          nixPaths = ''(list "${config.home.homeDirectory}/.nix-profile/bin" "/run/current-system/sw/bin")'';
          orgPath = "${lib.custom.pathToUserDir "documents"}/org";
        in
        ''
          (setq user-full-name "panacea"
                user-mail-address "${email}"
                epa-file-encrypt-to user-mail-address
                exec-path (append exec-path ${nixPaths})
                org-directory "${orgPath}"
                org-capture-templates
                  `(("p" "Protocol" entry (file+headline "${orgPath}/index.org" "Inbox")
                     "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")
                    ("L" "Protocol Link" entry (file+headline "${orgPath}/index.org" "Inbox")
                     "* %? [[%:link][%:description]] \nCaptured On: %U")))

          (setenv "PATH" (concat (getenv "PATH") ":" (mapconcat 'identity ${nixPaths} ":")))

          (after! tramp
            (setq tramp-remote-path (append tramp-remote-path ${nixPaths})))
        '';
    };

    programs.neovim = {
      enable = lib.mkDefault true;
      viAlias = true;
      vimAlias = true;
      plugins = with pkgs.vimPlugins; [
        vim-abolish
        vim-better-whitespace
        vim-commentary
        vim-eunuch
        vim-gitgutter
        vim-polyglot
        vim-repeat
        vim-sensible
        vim-sneak
        vim-surround
        vim-unimpaired
      ];
      extraConfig = ''
        set shell=/bin/sh
        set switchbuf=usetab,newtab
        set splitright splitbelow
        set hidden
        set tabstop=4 expandtab shiftwidth=4
        set smartindent ignorecase smartcase
        set undofile undodir=~/.local/share/nvim/undo
        set expandtab
        set title
        set number
        set signcolumn=yes
        set updatetime=300

        let mapleader="\<Space>"
        let maplocalleader=","

        let g:netrw_dirhistmax=0

        highlight Comment cterm=italic gui=italic

        nnoremap j gj
        nnoremap k gk
        vnoremap j gj
        vnoremap k gk
        nnoremap <Down> gj
        nnoremap <Up> gk
        vnoremap <Down> gj
        vnoremap <Up> gk
        inoremap <Down> <C-o>gj
        inoremap <Up> <C-o>gk
      '';
    };
  }

  (mkLinux {
    services.emacs = {
      enable = true;
      client.enable = true;
    };
  })
]
