{
  config,
  lib,
  pkgs,
  inputs,
  userConfig,
  customUserConfig,
  ...
}:

lib.mkMerge [
  {
    home.sessionVariables = {
      EDITOR = "nvim";
      TERM = "xterm-256color";
      # less: output raw ANSI color escape sequences
      LESS = "-R";
    };

    home.file = {
      ".npmrc".text = ''
        prefix = ${config.xdg.dataHome}/node-modules
      '';
    };

    programs.bash = {
      enable = lib.mkDefault true;
      shellAliases = {
        "ls" = "ls --hyperlink=auto";
        "p9" = "direnv exec $(dirname ${lib.custom.projectRootFile}) p9";
        "youtube-dl" = "yt-dlp";
        "yt" = "mpv --video=no --playlist=Documents/yt.m3u --shuffle";
      };
      bashrcExtra = ''
        sshpass () {
          ${lib.getExe pkgs.pass} -c ssh/id_$1/$(whoami)@$(hostname)
        }
        wifi-qr () {
          ${lib.getExe pkgs.qrencode} -t ANSIUTF8 "WIFI:T:WPA;S:$1;P:$(${lib.getExe pkgs.pass} wifi/$1);;"
        }
      '';
    };

    programs.direnv = {
      enable = lib.mkDefault true;
      nix-direnv.enable = lib.mkDefault true;
    };

    programs.home-manager.enable = lib.mkDefault true;

    programs.git = {
      enable = lib.mkDefault true;
      lfs.enable = lib.mkDefault true;
      userName = customUserConfig.fullName;
      userEmail = customUserConfig.email;
      extraConfig = {
        pull.rebase = false;
        gitlab.user = customUserConfig.email;
        diff.gpg.textconv = "${pkgs.gnupg}/bin/gpg --no-tty --decrypt";
      };
    };

    programs.gpg = {
      enable = lib.mkDefault true;
      settings = {
        default-key = "<${customUserConfig.gpgEmail}>";
      };
    };

    programs.nix-index.enable = lib.mkDefault true;

    programs.password-store.enable = lib.mkDefault true;

    programs.ssh = {
      enable = lib.mkDefault true;
      compression = lib.mkDefault true;
      matchBlocks =
        let
          sshDirectory = "${userConfig.home}/.ssh";
        in
        {
          "*.pan.ax 10.69.*.*" = {
            identityFile = "${sshDirectory}/id_pan.ax";
          };
          "gitlab.com" = {
            identityFile = "${sshDirectory}/id_gitlab.com";
          };
          "git.pan.ax git.src.pan.ax" = {
            identityFile = "${sshDirectory}/id_git.pan.ax";
          };
        };
      extraOptionOverrides = {
        user = userConfig.name;
        identitiesOnly = "yes";
      };
      extraConfig = ''
        Match Host *.pan.ax,!alexandria.pan.ax,10.69.1.*
          ProxyJump ${inputs.self.wireguardNetworks.pan0.peers.servers.alexandria.ip}
      '';
    };

    programs.starship = {
      enable = lib.mkDefault true;
      enableBashIntegration = true;
      settings = {
        format = ": $all";
        character.success_symbol = "[;](bold green)";
        character.error_symbol = "[;](bold red)";
        hostname.ssh_only = false;
        cmd_duration.disabled = true;
        package.disabled = true;
        line_break.disabled = true;
        nix_shell.disabled = true;
        git_status.disabled = true;
      };
    };

    programs.tmux = {
      enable = lib.mkDefault true;
      escapeTime = 0;
      terminal = "xterm-256color";
      keyMode = "vi";
      plugins = with pkgs.tmuxPlugins; [
        continuum
        copycat
        fzf-tmux-url
        open
        pain-control
        prefix-highlight
        resurrect
        sensible
        yank
      ];
      extraConfig = ''
        set -g mouse
        set -g base-index 1
        setw -g pane-base-index 1

        set -g status-left '''
        set -g status-right '%b %d %-H:%M '
        set -g status-justify centre

        set -g pane-border-style 'fg=black,bg=default'
        set -g pane-active-border-style 'fg=black,bg=default'
        set -g status-style 'fg=white,bg=black'

        # Make iTerm tmux integration work
        setw -g aggressive-resize off
      '';
    };

    programs.zellij.enable = lib.mkDefault true;

    programs.zsh.enable = lib.mkDefault false;
  }

  (lib.custom.mkLinux {
    services.gpg-agent = {
      enable = lib.mkDefault true;
      enableSshSupport = lib.mkDefault true;
      pinentryPackage = lib.mkDefault pkgs.pinentry-gtk2;
    };
  })
]
