{
  config,
  lib,
  pkgs,
  systemConfig,
  ...
}:

lib.custom.mkLinux {
  dconf = {
    enable = lib.mkDefault systemConfig.programs.dconf.enable;
    settings = {
      # GNOME settings
      "org/gnome/desktop/interface" = {
        enable-hot-corners = false;
        enable-animations = false;
        clock-show-date = true;
        clock-show-weekday = true;
      };
      "org/gnome/desktop/peripherals/mouse" = {
        accel-profile = "flat";
      };
      "org/gnome/desktop/media-handling" = {
        autorun-never = true;
      };
      "org/gnome/desktop/notifications" = {
        show-in-lock-screen = false;
        show-banners = false;
      };
      "org/gnome/desktop/peripherals/touchpad" = {
        natural-scroll = false;
        tap-to-click = true;
      };
      "org/gnome/desktop/privacy" = {
        remember-app-usage = false;
        remember-recent-files = false;
        remove-old-trash-files = true;
        remove-old-temp-files = true;
      };
      "org/gnome/desktop/search-providers" = {
        disable-external = true;
      };
      "org/gnome/desktop/session" = {
        idle-delay = 600;
      };
      "org/gnome/desktop/sound" = {
        event-sounds = false;
      };
      "org/gnome/desktop/wm/preferences" = {
        audible-bell = false;
        button-layout = "appmenu:minimize,maximize,close";
        titlebar-uses-system-font = true;
      };
      "org/gnome/shell/keybindings" = {
        switch-to-application-1 = [ ];
        switch-to-application-2 = [ ];
        switch-to-application-3 = [ ];
        switch-to-application-4 = [ ];
        switch-to-application-5 = [ ];
        switch-to-application-6 = [ ];
        switch-to-application-7 = [ ];
        switch-to-application-8 = [ ];
        switch-to-application-9 = [ ];
      };
      "org/gnome/desktop/wm/keybindings" = {
        close = [ "<Super>w" ];
        cycle-windows = [ "<Super>Tab" ];
        cycle-windows-backward = [ "<Shift><Super>Tab" ];
        switch-applications = [ ];
        switch-applications-backward = [ ];
        toggle-fullscreen = [ "<Super>f" ];
        switch-to-workspace-left = [ "<Super>braceleft" ];
        switch-to-workspace-right = [ "<Super>braceright" ];
        switch-to-workspace-1 = [ "<Super>1" ];
        switch-to-workspace-2 = [ "<Super>2" ];
        switch-to-workspace-3 = [ "<Super>3" ];
        switch-to-workspace-4 = [ "<Super>4" ];
        switch-to-workspace-5 = [ "<Super>5" ];
        switch-to-workspace-6 = [ "<Super>6" ];
        switch-to-workspace-7 = [ "<Super>7" ];
        switch-to-workspace-8 = [ "<Super>8" ];
        switch-to-workspace-9 = [ "<Super>9" ];
        switch-to-workspace-10 = [ "<Super>0" ];
        move-to-workspace-left = [ "<Shift><Super>braceleft" ];
        move-to-workspace-right = [ "<Shift><Super>braceright" ];
        move-to-workspace-1 = [ "<Shift><Super>1" ];
        move-to-workspace-2 = [ "<Shift><Super>2" ];
        move-to-workspace-3 = [ "<Shift><Super>3" ];
        move-to-workspace-4 = [ "<Shift><Super>4" ];
        move-to-workspace-5 = [ "<Shift><Super>5" ];
        move-to-workspace-6 = [ "<Shift><Super>6" ];
        move-to-workspace-7 = [ "<Shift><Super>7" ];
        move-to-workspace-8 = [ "<Shift><Super>8" ];
        move-to-workspace-9 = [ "<Shift><Super>9" ];
        move-to-workspace-10 = [ "<Shift><Super>0" ];
      };
      "org/gnome/settings-daemon/plugins/power" = {
        power-button-action = "interactive";
        sleep-inactive-ac-type = "nothing";
      };
      "org/gnome/nautilus/preferences" = {
        # 5000 MiB is the maximum, per https://gitlab.gnome.org/GNOME/nautilus/-/blob/master/data/org.gnome.nautilus.gschema.xml
        thumbnail-limit = 5000;
        show-image-thumbnails = "always";
      };
      "org/gtk/settings/file-chooser" = {
        show-hidden = "false";
      };

      # Other application settings
      "org/gnome/desktop/notifications/application/org-gnome-lollypop" = {
        enable = false;
      };
      "ca/desrt/dconf-editor" = {
        show-warning = false;
      };
      "com/gexperts/Tilix" = {
        warn-vte-config-issue = false;
      };
    };
  };

  gtk = {
    enable = lib.mkDefault true;
    theme.name = "Adwaita-dark";
    iconTheme = {
      name = "Adwaita";
      package = pkgs.adwaita-icon-theme;
    };
    font.name = "sans 12";
    gtk3.extraConfig = {
      "gtk-key-theme-name" = "Emacs";
    };
  };

  xdg = {
    userDirs.enable = lib.mkDefault true;
    configFile = {
      # Prevent warnings when rebuilding due to mimeapps.list
      # rewritten by other programs
      "mimeapps.list".force = true;
      "proton.conf".text = ''
        data = "${config.xdg.dataHome}/proton"
        steam = "${config.xdg.dataHome}/Steam"
      '';
    };
    mimeApps = {
      enable = lib.mkDefault true;
      defaultApplications =
        (lib.custom.associateMimeTypes "image" [
          "bmp"
          "gif"
          "jpeg"
          "png"
        ] "org.gnome.eog.desktop")
        // (lib.custom.associateMimeTypes "video" [
          "mp4"
          "quicktime"
          "webm"
          "x-matroska"
          "x-ms-wmv"
        ] "mpv.desktop")
        // (lib.custom.associateMimeTypes "audio" [
          "flac"
          "mp4"
          "mpeg"
          "x-wav"
        ] "mpv.desktop")
        // {
          "text/plain" = "nvim.desktop";
          "text/html" = "nvim.desktop";
          "text/ini" = "nvim.desktop";
          "application/x-ms-dos-executable" = "wine.desktop";
          "application/pdf" = "org.pwmt.zathura.desktop";
          "image/x-xcf" = "gimp.desktop";
          "inode/directory" = "nautilus.desktop";
          "x-scheme-handler/magnet" = "transmission-gtk.desktop";
          "x-scheme-handler/sgnl" = "signal-desktop.desktop";
          "x-scheme-handler/http" = "firefox.desktop";
          "x-scheme-handler/https" = "firefox.desktop";
          "x-scheme-handler/org-protocol" = "org-protocol.desktop";
        };
    };
  };
}
