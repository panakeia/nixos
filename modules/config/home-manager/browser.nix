{
  lib,
  pkgs,
  systemConfig,
  ...
}:

let
  profileName = "home";
in
lib.mkMerge [
  {
    home.sessionVariables.BROWSER = "firefox";

    programs.browserpass = {
      enable = lib.mkDefault true;
      browsers = [ "firefox" ];
    };

    programs.firefox = {
      enable = true;
      package =
        if pkgs.stdenv.hostPlatform.isDarwin then pkgs.nur.repos.toonn.apps.firefox else pkgs.firefox;
      profiles = {
        "${profileName}" = {
          id = 0;
          extensions = with pkgs.nur.repos.rycee.firefox-addons; [
            bitwarden
            browserpass
            cookie-autodelete
            firefox-translations
            greasemonkey
            multi-account-containers
            org-capture
            privacy-redirect
            temporary-containers
            tridactyl
            sidebery
            ublock-origin
            umatrix
            (pkgs.nur.repos.rycee.firefox-addons.buildFirefoxXpiAddon {
              pname = "yomichan";
              version = "22.10.23.0";
              addonId = "alex@foosoft.net";
              url = "https://github.com/FooSoft/yomichan/releases/download/22.10.23.0/a708116f79104891acbd-22.10.23.0.xpi";
              sha256 = "sha256-lSGJcgZcZE9bWcAtUeQZ4SyXv5wdbYDLvOFImjvnFa4=";
              meta = {
                homepage = "https://foosoft.net/projects/yomichan/";
                description = "Japanese pop-up dictionary extension";
                license = lib.licenses.gpl3;
                platform = lib.platforms.all;
              };
            })
          ];
          settings = {
            # behavior
            "browser.bookmarks.file" = "${lib.custom.pathToUserDir "documents"}/Firefox/bookmarks.html";
            "browser.bookmarks.autoExportHTML" = true;
            "browser.startup.homepage" = "about:blank";
            "browser.shell.checkDefaultBrowser" = false;
            "browser.tabs.closeWindowWithLastTab" = false;
            "browser.tabs.warnOnClose" = false;
            "network.protocol-handler.expose.org-protocol" = true;
            "media.default_volume" = "0.1";

            # privacy
            "app.normandy.enabled" = false;
            "app.shield.optoutstudies.enabled" = true;
            "beacon.enabled" = false;
            "browser.library.activity-stream.enabled" = false;
            "browser.newtabpage.activity-stream.feeds.topsites" = false;
            "browser.search.hiddenOneOffs" = "Amazon.com,Bing,eBay,Google,Twitter,Yahoo";
            "browser.urlbar.quicksuggest.enabled" = false;
            "experiments.activeExperiment" = false;
            "experiments.enabled" = false;
            "experiments.supported" = false;
            "extensions.formautofill.addresses.enabled" = false;
            "extensions.pocket.enabled" = false;
            "extensions.htmlaboutaddons.discover.enabled" = false;
            "extensions.htmlaboutaddons.recommendations.enabled" = false;
            "network.allow-experiments" = false;
            "network.dns.disablePrefetch" = true;
            "privacy.clearOnShutdown.cookies" = false;
            "privacy.clearOnShutdown.offlineApps" = true;
            "privacy.clearOnShutdown.sessions" = false;
            "privacy.clearOnShutdown.siteSettings" = true;
            "privacy.donottrackheader.enabled" = true;
            "privacy.history.custom" = true;
            "privacy.sanitize.sanitizeOnShutdown" = true;

            # visual
            "browser.tabs.drawInTitlebar" = false;
            "browser.search.region" = "US";
            "browser.search.suggest.enabled" = false;
            "browser.urlbar.placeholderName" = "DuckDuckGo";
            "devtools.theme" = "dark";
            "extensions.activeThemeID" = "firefox-compact-dark@mozilla.org";
            "identity.fxaccounts.account.device.name" = systemConfig.networking.hostName;
            "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
            "ui.systemUsesDarkTheme" = 1;

            # hardware acceleration
            # per https://wiki.archlinux.org/title/Firefox#Hardware_video_acceleration,
            # "starting from Firefox 97, only setting media.ffmpeg.vaapi.enabled to true should be necessary"
            "media.ffmpeg.vaapi.enabled" = true;
          };
          userChrome = ''
            #TabsToolbar {
              visibility: collapse !important;
            }
          '';
          userContent = ''
            :root {
              scrollbar-width: thin !important;
            }
          '';
        };
      };
    };

    xdg.configFile."tridactyl/tridactylrc".text = ''
      unbind <C-f>
      unbind <C-e>
    '';
  }

  (lib.custom.mkLinux {
    programs.chromium = {
      enable = lib.mkDefault true;
      package = pkgs.ungoogled-chromium;
      extensions = with lib.custom.chromeExtensions; [
        uBlockOrigin
        vimium
      ];
    };
  })
]
