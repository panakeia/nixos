{
  ...
}:

let
  domain = "pan.ax";
in
{
  services.transmission = {
    enable = true;
    settings = {
      download-dir = "/srv/tank/torrents";
      incomplete-dir = "/srv/tank/torrents/.incomplete";
      incomplete-dir-enabled = true;
      rpc-whitelist = "127.0.0.1,192.168.*.*,10.69.*.*";
      rpc-host-whitelist = "*.${domain}";
      rpc-bind-address = "127.0.0.1";
    };
  };
}
