{
  config,
  ...
}:

let
  domain = "pan.ax";
in
{
  sops.secrets = {
    forgejo_dbpassword = {
      owner = config.users.users.forgejo.name;
      group = config.users.users.forgejo.group;
    };
  };

  services.forgejo = {
    enable = true;
    # dump.enable = true;
    database = {
      user = "forgejo";
      # sh -c 'pwgen -s 20 1 > "forgejo_dbpassword"'
      passwordFile = "/run/secrets/forgejo_dbpassword";
    };
    settings = {
      server = {
        HTTP_ADDR = "127.0.0.1";
        ROOT_URL = "https://git.${domain}/";
        DOMAIN = "git.${domain}";
      };
      session = {
        COOKIE_SECURE = true;
      };
      webhook = {
        ALLOWED_HOST_LIST = "*.${domain}";
      };
    };
  };

  virtualisation.podman = {
    enable = true;
    dockerSocket.enable = true;
  };

  virtualisation.containers.storage.settings = {
    storage = {
      driver = "zfs";
      graphroot = "/var/lib/containers/storage";
      runroot = "/run/containers/storage";
    };
  };
}
