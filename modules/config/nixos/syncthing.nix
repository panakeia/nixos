{ config, lib, ... }:

let
  inherit (lib.custom) mkLinux;
  username = config.custom.users.pan.username;
  homeDir = config.users.users.${username}.home;

  deviceEnabled = lib.elem config.networking.hostName;
  deviceType = devices: if deviceEnabled devices then "sendreceive" else "receiveonly";
  referenceDeviceType =
    device: if config.networking.hostName == device then "sendonly" else "receiveonly";
in
mkLinux {
  services.syncthing = {
    enable = true;
    user = username;
    configDir = "${homeDir}/.config/syncthing";
    dataDir = "${homeDir}/.local/share/syncthing";
    settings = {
      devices = {
        arcadia.id = "LSLZDBE-EQOLX2B-HVQ6RHT-54EPM7K-KHHNUZZ-BHB6SXC-WIQG5ZW-MYRZDQW";
        argyre.id = "PZSW4WI-KKHWJQK-YX265ZZ-24M6ZHY-X7L56DG-JVLG46A-E52KLL2-EJ2IQAJ";
        alexandria.id = "WTS7VAY-Q47BAUU-JP7PFXZ-OQHUPH4-I3AY34K-LDHA2FR-47GGPXV-OV6DEAH";
        iphone.id = "JCKGMQ3-Q6G4HLV-GLHOVTQ-AVJG4DM-TXPXTFR-4LB4XE5-JNM2LHC-DTAXYQQ";
      };
      folders = {
        Games = rec {
          devices = [ "alexandria" ];
          path = "${homeDir}/Games";
          fsWatcherEnabled = true;
          type = referenceDeviceType "alexandria";
          enable = deviceEnabled devices;
        };
        Music = rec {
          devices = [
            "alexandria"
            "arcadia"
            "argyre"
          ];
          path = "${homeDir}/Music";
          fsWatcherEnabled = true;
          type = deviceType [
            "alexandria"
            "arcadia"
          ];
          enable = deviceEnabled devices;
        };
        Pictures = rec {
          devices = [
            "alexandria"
            "arcadia"
            "argyre"
          ];
          path = "${homeDir}/Pictures";
          fsWatcherEnabled = true;
          type = deviceType [
            "alexandria"
            "arcadia"
          ];
          enable = deviceEnabled devices;
        };
        Audio = rec {
          devices = [
            "arcadia"
            "argyre"
          ];
          path = "${homeDir}/Audio";
          fsWatcherEnabled = true;
          type = referenceDeviceType "arcadia";
          enable = deviceEnabled devices;
        };
        Documents = rec {
          devices = [
            "arcadia"
            "argyre"
            "iphone"
          ];
          path = "${homeDir}/Documents";
          fsWatcherEnabled = true;
          type = referenceDeviceType "arcadia";
          enable = deviceEnabled devices;
        };
        Public = rec {
          devices = [
            "arcadia"
            "argyre"
          ];
          path = "${homeDir}/Public";
          fsWatcherEnabled = true;
          type = deviceType [
            "arcadia"
            "argyre"
          ];
          enable = deviceEnabled devices;
        };
      };
    };
  };
}
