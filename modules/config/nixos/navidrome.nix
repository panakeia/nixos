{
  ...
}:

{
  services.navidrome = {
    enable = true;
    settings = {
      MusicFolder = "/srv/tank/Music";
      EnableExternalServices = false;
      EnableStarRating = false;
    };
  };
}
