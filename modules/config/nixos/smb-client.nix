{ config, inputs, ... }:

let
  mountRootDir = "/media/private";
  serverPeer = inputs.self.wireguardNetworks.pan0.peers.servers.alexandria;
in
{
  sops.secrets = {
    samba_credentials = { };
  };

  fileSystems.${mountRootDir} = {
    device = "//${serverPeer.ip}/private";
    fsType = "cifs";
    options = [
      (builtins.concatStringsSep "," [
        "x-systemd.automount"
        "noauto"
        "x-systemd.idle-timeout=60"
        "x-systemd.device-timeout=5s"
        "x-systemd.mount-timeout=5s"
        "credentials=/run/secrets/samba_credentials"
        "uid=${config.users.users.pan.name}"
        "gid=users"
        "rw"
      ])
    ];
  };

  home-manager.users.pan = {
    home.file = builtins.listToAttrs (
      map
        (dir: {
          name = dir;
          value = {
            source = config.home-manager.users.pan.lib.file.mkOutOfStoreSymlink "${mountRootDir}/${dir}";
          };
        })
        [
          "Games"
          "Videos"
        ]
    );

    xdg.userDirs = {
      videos = "${mountRootDir}/Videos";
    };
  };
}
