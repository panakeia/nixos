{
  config,
  lib,
  inputs,
  ...
}:

let
  inherit (inputs.self.wireguardNetworks.pan0) peers;
in
{
  services.unbound = {
    enable = true;
    resolveLocalQueries = true;
    settings = {
      server = {
        hide-identity = true;
        hide-version = true;
        harden-glue = true;
        harden-dnssec-stripped = true;
        use-caps-for-id = true;
        statistics-cumulative = true;
        extended-statistics = true;
        interface = [
          "127.0.0.1"
          "::1"
          peers.servers.${config.networking.hostName}.ip
        ];
        access-control = [
          "127.0.0.0/8 allow"
          "::1/128 allow"
          "192.168.0.0/16 allow"
          "10.69.0.0/16 allow"
        ];
        private-address = [
          "10.0.0.0/8"
          "100.64.0.0/10"
          "169.254.0.0/16"
          "172.16.0.0/12"
          "192.168.0.0/16"
          "fc00::/7"
          "fe80::/10"
        ];
        local-data = lib.concatMap lib.custom.dnsZoneToUnboundLocalData (
          builtins.attrValues inputs.self.dnsZones
        );
      };
      forward-zone = [
        {
          name = ".";
          forward-tls-upstream = true;
          forward-addr = lib.custom.dnsServers.withTls;
        }
      ];
    };
  };

  custom.networking.unbound-dns-blocking.enable = true;
}
