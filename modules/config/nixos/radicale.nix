{
  config,
  pkgs,
  ...
}:

{
  # for htpasswd
  environment.systemPackages = with pkgs; [ apacheHttpd ];

  sops.secrets = {
    radicale_htpasswd = {
      owner = config.users.users.radicale.name;
      group = config.users.users.radicale.group;
    };
  };

  services.radicale = {
    enable = true;
    settings = {
      server.hosts = [ "127.0.0.1:5232" ];
      auth = {
        # htpasswd -B -c "radicale_htpasswd"
        htpasswd_filename = "/run/secrets/radicale_htpasswd";
        htpasswd_encryption = "bcrypt";
      };
    };
  };
}
